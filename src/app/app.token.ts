import { InjectionToken } from '@angular/core';
import { Routes } from '@angular/router';

export const APP_ROUTING = new InjectionToken<Routes>('AppRouting');
