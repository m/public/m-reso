import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllRoutesResolver, PingReseauUpdateResolver } from '@metromobilite/m-features/core';
import { BeforeAfterComponent } from './pages/before-after/before-after.component';
import { HomeComponent } from './pages/home/home.component';
import { MapInitGuard } from '@m-ping/guards/map-init.guard';
import { LineDiffVisualizerComponent } from './pages/line-diff-visualizer/line-diff-visualizer.component';
import { GameOverComponent } from './pages/game-over/game-over.component';
import { VisualizationComponent } from './pages/visualization/visualization.component';
import { LineDetailComponent } from './pages/line-detail/line-detail.component';
import { ClusterDetailComponent } from './pages/cluster-detail/cluster-detail.component';
import { ClusterResolver } from './resolvers/cluster.resolver';
import { ClusterRoutesResolver } from './resolvers/cluster-routes.resolver';
import { CommunesResolver } from './resolvers/communes.resolver';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'visualisation',
    component: VisualizationComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'before-after'
      },
      {
        path: 'before-after',
        component: BeforeAfterComponent,
        canActivate: [MapInitGuard],
        resolve: {
          routes: AllRoutesResolver,
          pingUpdates: PingReseauUpdateResolver,
          communes: CommunesResolver
        },
        data: {
          title: 'M réso - Avant/Après',
          topLevel: true,
          item: {
            pingFeatureId: 'before-after-viewer',
            configuration: {
              beforeLayerIds: ["m-ping:before"],
              afterLayerIds: ["m-ping:after"]
            }
          }
        }
      },
      {
        path: 'detail-ligne/:lineId',
        component: LineDetailComponent,
        canActivate: [MapInitGuard],
        data: {
          title: 'M réso - Détail de ma ligne',
        },
        resolve: {
          routes: AllRoutesResolver,
          pingUpdates: PingReseauUpdateResolver
        },
      },
      {
        path: 'line-diff-visualizer/:oldLineId/:newLineId',
        component: LineDiffVisualizerComponent,
        canActivate: [MapInitGuard],
        data: {
          title: 'M réso - Comparateur de lignes'
        },
        resolve: {
          routes: AllRoutesResolver,
          pingUpdates: PingReseauUpdateResolver
        }
      },
      {
        path: 'detail-arret/:clusterId',
        component: ClusterDetailComponent,
        canActivate: [MapInitGuard],
        data: {
          title: 'M réso - Détail de votre arrêt',
        },
        resolve: {
          routes: AllRoutesResolver,
          pingUpdates: PingReseauUpdateResolver,
          cluster: ClusterResolver,
          clusterRoutes: ClusterRoutesResolver
        }
      }
    ]
  },
  {
    path: 'game-over',
    title: 'M réso - Bientôt (ne cliquez surtout pas !!!)',
    component: GameOverComponent,
    canActivate: [MapInitGuard],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
