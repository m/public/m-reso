import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransportationConfig } from 'src/app/models/transportation-config.model';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterModule } from '@angular/router';
import { LogoLinesDisplayComponent } from '../logo-lines-display/logo-lines-display.component';
import { RouteData } from '@m-ping';
import { RouteService } from 'src/app/services/route.service';
import { FindRouteDetailsPipe } from 'src/app/pipes/find-route-details.pipe';
import { ACTIVE_EXPANSION_PANELS } from 'src/app/models/reseau.model';
import { CoreModule } from '@metromobilite/m-features/core';
import { EFFECTIVE_AGENCY_IDS } from '../../utils/constants';
import { IncludesAnyPipe } from '@m-ping/pipes/includes-any.pipe';

@Component({
  selector: 'app-ping-updates-accordion',
  standalone: true,
  templateUrl: './ping-updates-accordion.component.html',
  styleUrls: ['./ping-updates-accordion.component.scss'],
  imports: [CommonModule, MatExpansionModule, RouterModule, LogoLinesDisplayComponent, FindRouteDetailsPipe, CoreModule, IncludesAnyPipe]
})
export class PingUpdatesAccordionComponent implements OnInit {

  @Input() routeIds: string[] = [];
  @Input() activatePanelSelectionHistory: boolean = false;
  @Input() expandAllPanels: boolean = false;

  transportationConfig: TransportationConfig;
  filteredTransportationConfig = {};
  activeExpansionPanels: { [key: string]: boolean } = {};
  changedRoutes: RouteData[] = [];
  unchangedRoutes: RouteData[] = [];
  effectiveAgencyIds: string[] = EFFECTIVE_AGENCY_IDS;

  constructor(public routeService: RouteService) { }

  ngOnInit(): void {
    this.transportationConfig = this.reorderObjectByOrder(this.routeService.currentRouteStaticData.data.pingUpdates.updates);
    this.separateRoutes();

    if (this.activatePanelSelectionHistory) {
      const storedActiveExpansionPanels = JSON.parse(localStorage.getItem(ACTIVE_EXPANSION_PANELS));
      if (!storedActiveExpansionPanels) {
        this.initActivePanels();
      } else {
        this.activeExpansionPanels = { ...storedActiveExpansionPanels };
      }
    }
  }

  separateRoutes() {
    const effectiveRoutes = this.routeService.currentRouteStaticData.data.routes;
    const configShortNames = this.getAllConfigShortNames();

    for (const route of effectiveRoutes) {
      if (this.isRouteChanged(route.id)) {
        this.changedRoutes.push(route);
      } else {
        const agencyId = route.id.split(':')[0];
        if (!configShortNames.has(route.shortName) && ['OLD'].includes(agencyId)) {
          this.unchangedRoutes.push(route);
        }
      }
    }
    this.filterRoutes();
  }

  private isRouteChanged(routeId: string): boolean {
    let changed = false;

    const categories = Object.values(this.transportationConfig);
    for (const category of categories) {
      for (const data of category.data) {
        if (data.id === routeId ||  data.oldId === routeId) {
          changed = true;
          break;
        }
      }
      if (changed) break;
    }

    return changed;
  }

  private getAllConfigShortNames(): Set<string> {
    const ids = new Set<string>([]) 
    for (const category of Object.values(this.transportationConfig)) {
      for (const data of category.data) {
        ids.add(data.id.split(':')[1]);
        if (data.oldId) ids.add(data.oldId.split(':')[1]);
      }
    }
    return ids;
  }

  reorderObjectByOrder(config: TransportationConfig): TransportationConfig {
    const entries = Object.entries(config);

    const sortedEntries = entries.sort((a, b) => a[1].order - b[1].order);

    const sortedObject = {};
    for (const [key, value] of sortedEntries) {
      sortedObject[key] = value;
    }

    return sortedObject as TransportationConfig;
  }

  private filterRoutes() {
    if (this.routeIds.length === 0) {
      this.filteredTransportationConfig = this.transportationConfig;
      return;
    }

    for (const [key, category] of Object.entries(this.transportationConfig)) {
      const filteredData = category.data.filter(item => this.routeIds.includes(item.id) || this.routeIds.includes(item.oldId));
      if (filteredData.length > 0) {
        this.filteredTransportationConfig[key] = { ...category, data: filteredData };
      }
    }

    this.unchangedRoutes = this.unchangedRoutes.filter((route: RouteData) => { return this.routeIds.includes(route.id) })
  }

  private initActivePanels() {
    Object.keys(this.filteredTransportationConfig).forEach((key: string) => {
      this.activeExpansionPanels[key] = false;
    });
  }

  trackByFn(index: any, item: any) {
    return item.key;
  }

  changeExpansionPanelState(key: string, state: boolean) {
    if (this.activatePanelSelectionHistory) {
      this.activeExpansionPanels[key] = state;
      localStorage.setItem(ACTIVE_EXPANSION_PANELS, JSON.stringify(this.activeExpansionPanels));
    }
  }

}
