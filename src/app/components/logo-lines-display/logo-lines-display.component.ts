import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@metromobilite/m-features/core';
import { RouteData } from '@m-ping/features';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { MatRippleModule } from '@angular/material/core';
import { IncludesAnyPipe } from '@m-ping/pipes/includes-any.pipe';

@Component({
  selector: 'app-logo-lines-display',
  standalone: true,
  imports: [CommonModule, IncludesAnyPipe, CoreModule, MatIconModule, MatRippleModule],
  templateUrl: './logo-lines-display.component.html',
  styleUrls: ['./logo-lines-display.component.scss']
})
export class LogoLinesDisplayComponent {
  @Input() oldLineDetails: RouteData;
  @Input() newLineDetails: RouteData;

  constructor (private router: Router) { }
}
