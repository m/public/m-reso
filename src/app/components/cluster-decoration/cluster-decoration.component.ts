import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeToggleService } from '@m-ping/services/theme-toggle.service';

@Component({
  selector: 'app-cluster-decoration',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './cluster-decoration.component.html',
  styleUrls: ['./cluster-decoration.component.scss']
})
export class ClusterDecorationComponent {
  constructor (public themeService: ThemeToggleService){}
 
}
