import { Component, ElementRef, Input, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClusterData, PingMapService, PingService } from '@m-ping';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { PingClustersSource } from '@m-ping/features/pois/map/source/pois.source';
import VectorSource from 'ol/source/Vector';
import { Feature } from 'ol';
import { ClusterDecorationComponent } from '../cluster-decoration/cluster-decoration.component';
import { GlobalService } from '@m-ping/services/global.service';

@Component({
  selector: 'app-cluster-list-display',
  standalone: true,
  imports: [CommonModule, MatListModule, MatIconModule],
  templateUrl: './cluster-list-display.component.html',
  styleUrls: ['./cluster-list-display.component.scss']
})
export class ClusterListDisplayComponent implements OnInit, OnDestroy {

  @Input() clusters: ClusterData[];
  @Input() lineId: string;
  @Input() strokeColor?: string = 'inherit';
  @Input() fillColor?: string = 'white';

  @ViewChildren('listItem', { read: ElementRef }) listItems: QueryList<ElementRef>;

  private activeItemIndex: number | null = null;
  private unSubscriber = new Subject<void>();


  selectedClusterCode: string | null = null;

  constructor (private pingMapService: PingMapService, private poiClustersSource: PingClustersSource, private pingService: PingService, private globalService: GlobalService) { }

  ngOnInit(): void {
    this.pingMapService.featureSelected$.pipe(takeUntil(this.unSubscriber)).subscribe((feature: Feature) => {
      if (feature) {
        const featureId = feature.getId();
        this.selectedClusterCode = feature.get('code') as string;
        const index = this.clusters.findIndex(cluster => cluster.code === featureId);
        if (index !== -1) {
          this.activateItem(index);
        }
      }
    });
  }

  ngAfterViewInit() {
    this.listItems.changes.subscribe((items: QueryList<ElementRef>) => {
      if (this.activeItemIndex !== null && items.length > this.activeItemIndex) {
        this.activateItem(this.activeItemIndex);
      }
    });

    if (this.activeItemIndex !== null && this.listItems.length > this.activeItemIndex) {
      this.activateItem(this.activeItemIndex);
    }
  }

  activateItem(index: number): void {
    this.clearActiveListItems();
    const elementRef = this.listItems.toArray()[index];
    if (elementRef && elementRef.nativeElement) {
      elementRef.nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'start'
      });
      elementRef.nativeElement.classList.add('active');
    } else {
      console.error('Element not found for index', index);
    }
  }

  ngOnDestroy(): void {
    this.clearActiveListItems();
    this.unSubscriber.next();
  }


  toggleSelectedCluster(cluster: ClusterData) {
    const clusterId = cluster.code;

    if (this.selectedClusterCode === cluster.code) {
      if (this.pingService.selectedFeature) {
        this.pingService.clearSelectedFeature();
      }
      this.selectedClusterCode = null;
      this.pingMapService.updateSelectedFeature(null);
      return;
    }

    if (this.pingService.selectedFeature) {
      this.pingService.clearSelectedFeature();
    }

    this.pingService.selectedFeature = (this.poiClustersSource.getSource() as VectorSource).getFeatureById(clusterId);
    if (this.pingService.selectedFeature) {
      this.pingService.selectedFeature.set('selected', true);
      this.pingMapService.updateSelectedFeature(this.pingService.selectedFeature);
    }
    this.selectedClusterCode = cluster.code;
    this.globalService.goDown();
  }

  clearSelection() {
    this.selectedClusterCode = null;
    this.clearActiveListItems();
    if (this.pingService.selectedFeature) {
      this.pingService.clearSelectedFeature();
    }
  }

  clearActiveListItems() {
    this.listItems.forEach(item => {
      item.nativeElement.classList.remove('active');
    });
  }
}
