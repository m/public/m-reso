import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PingUpdateDescription } from 'src/app/models/reseau.model';

@Component({
  selector: 'app-description-list-display',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './description-list-display.component.html',
  styleUrls: ['./description-list-display.component.scss']
})
export class DescriptionListDisplayComponent {

  @Input() descriptions: PingUpdateDescription[];

}
