export interface CurrentRouteStaticData {
  data?: any;
  params?: any;
  queryParams?: any;
  routePingConfig?: any;
  configCategory?: any;
}

export interface PingUpdateDescription {
  label: string;
  content: string;
}


export const ACTIVE_EXPANSION_PANELS = 'activeExpansionPanels';
