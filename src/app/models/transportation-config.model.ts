export interface LineDescription {
  label: string;
  content: string;
}

export interface LineData {
  id?: string;
  oldId?: string;
  newId?: string;
  description: LineDescription[];
}

export interface LineCategory {
  category: string;
  order: number;
  displayOldLine?: boolean;
  lineDiff?: boolean;
  data: LineData[];
}

export interface TransportationConfig {
  nouvellesLignes: LineCategory;
  lignesModifieesTrace: LineCategory;
  lignesModifieesNom: LineCategory;
  lignesModifieesTraceEtNom: LineCategory;
  lignesModifieesHoraires: LineCategory;
}


