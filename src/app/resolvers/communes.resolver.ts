import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Commune } from '@m-ping';
import { Observable, map } from 'rxjs';
import { ApiMService } from 'src/app/services/api-m.service';

@Injectable({
  providedIn: 'root'
})
export class CommunesResolver implements Resolve<any> {
  constructor (private apiService: ApiMService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Commune[]> {
    return this.apiService.getCommunes().pipe(map(communes => this.sortCommunes(communes)));
  }

  private sortCommunes(communes: Commune[]): Commune[] {
    return communes.sort((a, b) => a.commune.localeCompare(b.commune));
  }
}
