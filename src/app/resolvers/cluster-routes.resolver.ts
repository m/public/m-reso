import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RouteData } from '@m-ping';
import { Observable, map } from 'rxjs';
import { ApiMService } from 'src/app/services/api-m.service';
import { EFFECTIVE_AGENCY_IDS } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class ClusterRoutesResolver implements Resolve<any> {
  constructor (private apiService: ApiMService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<RouteData[]> {
    const clusterId = route.paramMap.get('clusterId');

    return this.apiService.getRoutesForCluster(clusterId).pipe(
      map(clusterRoutes => {
        return clusterRoutes.filter((route: RouteData) => { return !['TAD', 'BUL', 'MCO', 'FUN', 'SNC', 'SCOL', 'PMR', 'NAVETTE'].includes(route.type) && EFFECTIVE_AGENCY_IDS.includes(route.id.split(':')[0]) });
      }));
  }
}
