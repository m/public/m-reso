import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, map } from 'rxjs';
import { ApiMService } from 'src/app/services/api-m.service';

@Injectable({
  providedIn: 'root'
})
export class ClusterResolver implements Resolve<any> {
  constructor (private apiService: ApiMService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const clusterId = route.paramMap.get('clusterId');
    return this.apiService.getClusterFromCode(clusterId).pipe(map(response => { return response.features[0]}));
  }
}
