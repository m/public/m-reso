let _domain = 'https://data-pp.mobilites-m.fr';

if (['reso.mobilites-m.fr'].includes(window.location.hostname)) {
	_domain = 'https://data.mobilites-m.fr';
}


export const domain = _domain;
