let _matomoConfig = {
    scriptUrl: 'https://www.mobilites-m.fr/stats/piwik.js',
    trackerUrl: 'https://www.mobilites-m.fr/stats/piwik.php',
    id: 24
};

if (['reso.mobilites-m.fr'].includes(window.location.hostname)) {
	_matomoConfig.id = 22;
}


export const matomoConfig = _matomoConfig;