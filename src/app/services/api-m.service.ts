import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LinesService, StringHelper } from '@metromobilite/m-features/core';
import { PoiCollection } from '@metromobilite/m-features/reference';
import { ClusterData, Commune, RouteData } from '@m-ping';

@Injectable({
  providedIn: 'root'
})
export class ApiMService {
  public urlApi = `@domain/@api`;


  constructor (
    public http: HttpClient
  ) { }


  get(path: string, options?: any): Observable<any> {
    return this.http.get(this.urlApi + path, options)
      .pipe(
        catchError(this.handleError)
      );
  }

  getClustersOfRoute(routeId: string): Observable<ClusterData[]> {
    const path = `/routers/default/index/routes/${routeId}/clusters`;
    return this.get(path);
  }

  getRoutesForCluster(clusterId: string): Observable<RouteData[]> {
    const path = `/routers/default/index/clusters/${clusterId}/routes?allRoutes=true`;
    return this.get(path);
  }

  getClusterFromCode(code: string): Observable<PoiCollection> {
    const path = `/points/json?codes=${code}&types=clusters&epci=All`;
    return this.get(path);
  }

  search(term: string): Observable<string[]> {
    if (term === '') {
      return of([]);
    }

    const path = `/find/json?query=${term}&types=clusters&epci=All&includeOldAgencyId=true`;

    return this.get(path).pipe(
      catchError(error => {
        console.error('Search error', error);
        return of([]);
      })
    );
  }

  getCommunes(): Observable<Commune[]> {
    const path = `/city/json`;
    return this.get(path).pipe(
      catchError(error => {
        console.error('Search error', error);
        return of([]);
      })
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend returned code ${error.status}, body was:`, error.error);
    }
    return throwError('Une erreur est survenue; reessayez plus tard.');
  }
}
