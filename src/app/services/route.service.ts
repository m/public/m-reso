import { Injectable } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { BehaviorSubject } from "rxjs";
import { CurrentRouteStaticData } from "../models/reseau.model";
import { RouteData } from "@m-ping";
import { EFFECTIVE_AGENCY_IDS } from "../utils/constants";
import { ThemeToggleService } from "@m-ping/services/theme-toggle.service";
import { THEMES } from "@metromobilite/m-features/core";
import { Title } from "@angular/platform-browser";
import { MatomoTracker } from "ngx-matomo";


@Injectable({
  providedIn: 'root'
})
export class RouteService {
  currentRouteStaticData: CurrentRouteStaticData = {
    data: {},
    params: {},
    queryParams: {},
    routePingConfig: {},
    configCategory: {}
  }

  currentRouteStaticData$ = new BehaviorSubject<CurrentRouteStaticData>({});
  constructor(private router: Router, private activatedRoute: ActivatedRoute, public themeService: ThemeToggleService, 
    public titleService: Title, private matomoTracker: MatomoTracker) {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          this.currentRouteStaticData = {
            data: {},
            params: {},
            queryParams: {},
          };
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        tap(route => {
          if (this.router.getCurrentNavigation()?.extras.state) {
            const navigation = this.router.getCurrentNavigation();
            this.currentRouteStaticData.configCategory = navigation.extras.state["configCategory"];
          }
          route.paramMap.subscribe(params => {
            params.keys.forEach(key => this.currentRouteStaticData.params[key] = params.get(key));
          });
          route.queryParamMap.subscribe(queryParams => {
            queryParams.keys.forEach(key => {
              if (key === 'theme') {
                this.themeService.toggleTheme(queryParams.get(key) as THEMES);
              }
              this.currentRouteStaticData.queryParams[key] = queryParams.get(key)
            });
          });
        }),
        mergeMap(route => route.data),
      )
      .subscribe((data: any) => {
        // MATOMO
        this.trackPageView(data.title);
        
        if (data && data.routes) {
          data.routes = data.routes
            .filter((route: RouteData) => { return !['TAD', 'BUL', 'MCO', 'FUN', 'SNC', 'SCOL', 'PMR', 'NAVETTE'].includes(route.type) && EFFECTIVE_AGENCY_IDS.includes(route.id.split(':')[0]) })
        }
        this.currentRouteStaticData.data = data;
        this.emitNewRouteConfiguration();
      });
  }

  trackPageView(title: string) {
    this.matomoTracker.setCustomUrl(this.router.url);
    this.matomoTracker.trackPageView(title);
  }

  emitNewRouteConfiguration() {
    this.currentRouteStaticData$.next(this.currentRouteStaticData);
  }
}
