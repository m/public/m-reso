import { Pipe, PipeTransform } from '@angular/core';
import { RouteService } from '../services/route.service';

@Pipe({
  name: 'findPingUpdates',
  standalone: true
})
export class FindPingUpdatesPipe implements PipeTransform {

  constructor (private routeService: RouteService) { }

  transform(section: any, id: string): any {
    if (section) {
      const dataEntry = section.data.find((entry: any) => entry.id === id || entry.oldId === id);
      if (dataEntry) {
        this.routeService.currentRouteStaticData.data.description = dataEntry.description;
        return {
          category: section.category,
          displayOldLine: section.displayOldLine || false,
          oldId: dataEntry.oldId || null
        };
      }
    }
    if (id) {
      return {
        category: 'Ligne sans modifications',
        displayOldLine: false,
        oldId: id
      };
    }

    return null;

  }
}
