import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeS',
  standalone: true
})
export class RemoveSPipe implements PipeTransform {

  transform(value: string): string {
    if (!value) return value;
    return value.split(' ').map(word => word.endsWith('s') ? word.slice(0, -1) : word).join(' ');
  }

}
