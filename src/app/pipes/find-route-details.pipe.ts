import { Pipe, PipeTransform } from '@angular/core';
import { RouteData } from '@m-ping/features';
import { LineData } from '../models/transportation-config.model';

@Pipe({
  name: 'findRouteDetails',
  standalone: true
})
export class FindRouteDetailsPipe implements PipeTransform {

  transform(allRoutes: RouteData[], routeID: string): RouteData | undefined {
    return allRoutes.find(route => route.id === routeID);
  }
}
