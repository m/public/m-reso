import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatSidenavModule} from '@angular/material/sidenav';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MFeaturesModule } from '@metromobilite/m-features';
import { HttpClientModule } from '@angular/common/http';
import { MLayersModule } from '@metromobilite/m-map/m-layers';
import { CoreModule } from '@metromobilite/m-features/core';
import { MPingMapComponent, PingModule } from '@m-ping';
import { HomeComponent } from './pages/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { domain } from './helpers/domain.helper';
import { MatomoModule } from 'ngx-matomo';
import { matomoConfig } from './helpers/matomo.helper';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatomoModule.forRoot({
			scriptUrl: matomoConfig.scriptUrl,
			trackers: [
				{
				trackerUrl: matomoConfig.trackerUrl,
				siteId: matomoConfig.id
				}
			],
			routeTracking: {
				enable: true
      }
		}),
    MFeaturesModule.forRoot({
      domain
      // domain: 'http://localhost:5400',
    }),
    MLayersModule,
    PingModule,
    MPingMapComponent,
    HomeComponent,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
