// Angular imports
import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClusterData, PingMapService, PingFeature, PingService } from '@m-ping';
import { RouteService } from 'src/app/services/route.service';

// Material imports
import { MatTabsModule } from '@angular/material/tabs';
import { LineDiffVisualizerSource } from '@m-ping/features/line-diff-visualizer/map/source/line-diff-visualizer.source';
import { catchError, forkJoin, map, of } from 'rxjs';
import { ApiMService } from 'src/app/services/api-m.service';
import { ClusterListDisplayComponent } from 'src/app/components/cluster-list-display/cluster-list-display.component';
import { DescriptionListDisplayComponent } from 'src/app/components/description-list-display/description-list-display.component';
import { PingUpdateDescription } from 'src/app/models/reseau.model';
import { FindRouteDetailsPipe } from 'src/app/pipes/find-route-details.pipe';
import { Line, LinesService } from '@metromobilite/m-features/core';
import { PingClustersSource } from '@m-ping/features/pois/map/source/pois.source';
import VectorSource from 'ol/source/Vector';




// Type definitions
interface LayerConfig {
  mapLayerId: string;
  color?: string;
  dashPattern?: string;
  dashEnabled?: boolean;
  opacity?: number;
  width?: number;
}

interface TabLayerConfig {
  label: string;
  routeID: string;
  config?: LayerConfig;
}


@Component({
  selector: 'app-line-diff-visualizer',
  standalone: true,
  imports: [CommonModule, MatTabsModule, ClusterListDisplayComponent, DescriptionListDisplayComponent, FindRouteDetailsPipe],
  templateUrl: './line-diff-visualizer.component.html',
  styleUrls: ['./line-diff-visualizer.component.scss']
})
export class LineDiffVisualizerComponent implements OnInit {

  layerConfigs: TabLayerConfig[] = [];
  layerConfigsDeepCopy: TabLayerConfig[];
  clusters: { [key: string]: ClusterData[] } = {};
  oldLineId: string;
  newLineId: string;

  oldLineColorConfig: any = {}

  descriptions: PingUpdateDescription[] = [];
  oldLine: Line;
  newLine: Line;

  @ViewChild('clusterListNew') clusterListNewComponent: ClusterListDisplayComponent;
  @ViewChild('clusterListOld') clusterListOldComponent: ClusterListDisplayComponent;


  constructor (private pingService: PingService, private routeService: RouteService,
    private lineDiffVisualizerSource: LineDiffVisualizerSource,
    private apiMService: ApiMService, private lineService: LinesService, private pingMapService: PingMapService, private pingClusterSource: PingClustersSource) {
    this.oldLineColorConfig = {
      color: '95989A',
      textColor: '000'
    };
  }

  ngOnInit(): void {
    this.oldLineId = this.routeService.currentRouteStaticData.params.oldLineId;
    this.newLineId = this.routeService.currentRouteStaticData.params.newLineId;
    this.descriptions = this.routeService.currentRouteStaticData.data.description;
    this.newLine = this.lineService.find(this.newLineId);

    const pingFeature: PingFeature = {
      pingFeatureId: 'line-diff-visualizer',
      configuration: {
        oldLine: this.oldLineId,
        newLine: this.newLineId
      }
    }

    this.fetchRoutesClusters(this.oldLineId, this.newLineId).subscribe();
    this.pingService.activateFeature(pingFeature);
  }

  ngOnDestroy(): void {
    this.pingMapService.hidePoisLayer();
    this.lineDiffVisualizerSource.clear();
    this.pingService.clearView();
  }

  fetchRoutesClusters(oldLineId: string, newLineId: string) {
    return forkJoin({
      oldClusters: this.apiMService.getClustersOfRoute(oldLineId),
      newClusters: this.apiMService.getClustersOfRoute(newLineId)
    }).pipe(
      map(({ oldClusters, newClusters }) => {
        this.clusters[oldLineId] = [];
        this.clusters[newLineId] = [];
        this.clusters[oldLineId] = oldClusters;
        this.clusters[newLineId] = newClusters;
        this.onTabChange(0);
        return true;
      }),
      catchError(err => {
        console.error('Error fetching data:', err);
        return of(false);
      })
    );
  }

  onTabChange(index: number) {
    // Hide all POIs first
    this.pingMapService.hidePoisLayer();

    // Reset selections when switching tabs
    this.clusterListNewComponent?.clearSelection();
    this.clusterListOldComponent?.clearSelection();

    if (index === 0) {
      this.pingClusterSource.activateFeatures(this.clusters[this.newLineId], this.newLine.textColor, this.newLine.color, false);
      this.changeNewLineFeatureState(true);
    } else if (index === 1) {
      this.pingClusterSource.activateFeatures(this.clusters[this.oldLineId], this.oldLineColorConfig.textColor, this.oldLineColorConfig.color, true);
      this.changeNewLineFeatureState(false);
    }
    this.pingMapService.showPoisLayer();
  }

  changeNewLineFeatureState(state: boolean) {
    const oldLineFeature = (this.lineDiffVisualizerSource.getSource() as VectorSource).getFeatureById(this.oldLineId);
    const newLineFeature = (this.lineDiffVisualizerSource.getSource() as VectorSource).getFeatureById(this.newLineId);
    if (newLineFeature) newLineFeature.set('activated', state);
    if (oldLineFeature) oldLineFeature.set('activated', !state);
  }
}
