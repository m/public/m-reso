import { Component, OnDestroy, OnInit, ViewChildren, QueryList, ElementRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MLayersModule } from '@metromobilite/m-map/m-layers';
import { Commune, PingMapService, PingService, RouteData } from '@m-ping';
import { RouteService } from 'src/app/services/route.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterModule } from '@angular/router';
import { LogoLinesDisplayComponent } from 'src/app/components/logo-lines-display/logo-lines-display.component';
import { LogoMComponent } from 'src/app/components/logo-m/logo-m.component';
import { FindRouteDetailsPipe } from 'src/app/pipes/find-route-details.pipe';
import { TransportationConfig } from 'src/app/models/transportation-config.model';
import { MPingBeforeAfterLinesSource } from '@m-ping/features/before-after-map-viewer/map/source/before-after.source';
import { Subject, debounceTime, distinctUntilChanged, switchMap, takeUntil } from 'rxjs';
import { ApiMService } from 'src/app/services/api-m.service';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { PingUpdatesAccordionComponent } from 'src/app/components/ping-updates-accordion/ping-updates-accordion.component';
import { ClusterDecorationComponent } from 'src/app/components/cluster-decoration/cluster-decoration.component';
import { MatTabsModule, MatTabChangeEvent } from '@angular/material/tabs';
import { CommuneZoneSource } from '@m-ping/features/communes-zones/map/source/communes-zones.source';

@Component({
  selector: 'app-before-after',
  standalone: true,
  imports: [CommonModule, MLayersModule, PingUpdatesAccordionComponent, MatExpansionModule, LogoLinesDisplayComponent, FindRouteDetailsPipe,
    LogoMComponent, RouterModule, FormsModule, MatInputModule, MatButtonModule, MatIconModule, MatListModule, ClusterDecorationComponent, MatTabsModule],
  templateUrl: './before-after.component.html',
  styleUrls: ['./before-after.component.scss']
})
export class BeforeAfterComponent implements OnInit, OnDestroy, AfterViewInit {
  private unSubscriber = new Subject<void>();
  transportationConfig: TransportationConfig;
  changedRoutes: RouteData[] = [];
  unchangedRoutes: RouteData[] = [];
  activeExpansionPanels: { [key: string]: boolean } = {};

  // Search feature
  searchTerm: string = '';
  results: any[] = [];
  displayedResults: any[] = [];
  showAll: boolean = false;
  private searchSubject: Subject<any> = new Subject();

  communes: Commune[] = [];
  selectedCommune: string | null = null;

  @ViewChildren('listItem', { read: ElementRef }) listItems: QueryList<ElementRef>;

  constructor(
    private pingService: PingService,
    public routeService: RouteService,
    private communeZoneSource: CommuneZoneSource,
    private mPingBeforeAfterLinesSource: MPingBeforeAfterLinesSource,
    private apiMService: ApiMService,
    private mPingMapService: PingMapService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.searchSubject.pipe(
      takeUntil(this.unSubscriber),
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => term.length >= 3 ? this.apiMService.search(term) : [])
    ).subscribe(results => {
      this.results = results.features.filter(feature => {
        const id = feature.properties['code'];
        const agencyId = id.split(':')[0];

        return !['BUL', 'MCO', 'FUN', 'SNC', 'C38'].includes(agencyId);
      });
      this.updateDisplayedResults();
    });
  }

  ngOnInit(): void {
    this.pingService.activateFeature(this.routeService.currentRouteStaticData.data.item);
    this.mPingBeforeAfterLinesSource.addLinesToSource().subscribe((response) => { });
    this.communes = this.routeService.currentRouteStaticData.data.communes;
    
    // Check for the zone parameter and activate the corresponding commune
    this.route.queryParams.pipe(takeUntil(this.unSubscriber)).subscribe(params => {
      const zone = params['zone'];
      if (zone) {
        this.selectedCommune = zone;
        this.toggleSelectedCommune(zone);
      }
    });
  }

  ngAfterViewInit(): void {
    this.listItems.changes.pipe(takeUntil(this.unSubscriber)).subscribe(() => {
      this.scrollToActiveItem();
    });
    this.scrollToActiveItem();
  }

  ngOnDestroy(): void {
    this.pingService.clearView();
    this.mPingMapService.hidePolygonLayer();
    this.unSubscriber.next();
    this.unSubscriber.complete();
  }

  onSearch(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.searchSubject.next(target.value);
  }

  showMore(): void {
    this.showAll = true;
    this.updateDisplayedResults();
  }

  showLess(): void {
    this.showAll = false;
    this.updateDisplayedResults();
  }

  updateDisplayedResults(): void {
    if (this.showAll) {
      this.displayedResults = this.results;
    } else {
      this.displayedResults = this.results.slice(0, 3);
    }
  }

  trackByFn(index: any, item: any) {
    return item.key;
  }

  trackByCommune(index: number, commune: Commune): string {
    return commune.ref_insee;
  }

  clearSearch() {
    this.searchTerm = '';
    this.results = this.displayedResults = [];
  }

  toggleSelectedCommune(ref_insee: string) {
    this.selectedCommune = ref_insee;
    console.log(`Fetching data for commune: ${ref_insee}`);
    
    // Update the URL with the selected commune ref_insee
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { zone: ref_insee },
      queryParamsHandling: 'merge', // merge with existing query parameters
      replaceUrl: true // avoid pushing new state to the history
    });
    
    this.communeZoneSource.getData(ref_insee).pipe(
      takeUntil(this.unSubscriber)
    ).subscribe({
      next: (result) => {
        console.log(`Data fetched for commune: ${ref_insee}`, result);
        if (result) {
          this.communeZoneSource.changed();
          this.mPingMapService.showPolygonLayer();
          this.mPingMapService.centerOnZone();
          this.scrollToActiveItem(); // Scroll to the active item after the data is fetched
        }
      },
      error: (err) => {
        console.error(`Error fetching data for commune: ${ref_insee}`, err);
      }
    });
  }

  onTabChange(event: MatTabChangeEvent): void {
    if (event.index === 1) { // Assuming "Communes" tab is at index 1
      setTimeout(() => this.scrollToActiveItem(), 0); // Defer scrolling to ensure DOM is updated
    }
  }

  scrollToActiveItem(): void {
    if (this.selectedCommune && this.listItems) {
      const activeItem = this.listItems.find(item => item.nativeElement.classList.contains('active'));
      if (activeItem) {
        activeItem.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
      }
    }
  }
}
