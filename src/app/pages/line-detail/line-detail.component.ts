import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MLayersLinesSource, MapFiltersService } from '@metromobilite/m-map/m-layers';
import { RouteService } from 'src/app/services/route.service';
import { Router } from '@angular/router';
import { Geometry } from 'ol/geom';
import { Feature } from 'ol';
import { Extent } from 'ol/extent';
import { ClusterData, PingMapService } from '@m-ping';
import VectorSource from 'ol/source/Vector';
import { Line, LinesService } from '@metromobilite/m-features/core';
import { ApiMService } from 'src/app/services/api-m.service';
import { PingUpdateDescription } from 'src/app/models/reseau.model';
import { ClusterListDisplayComponent } from 'src/app/components/cluster-list-display/cluster-list-display.component';
import { DescriptionListDisplayComponent } from 'src/app/components/description-list-display/description-list-display.component';
import { PingClustersSource } from '@m-ping/features/pois/map/source/pois.source';

@Component({
  selector: 'app-line-detail',
  standalone: true,
  imports: [CommonModule, ClusterListDisplayComponent, DescriptionListDisplayComponent],
  templateUrl: './line-detail.component.html',
  styleUrls: ['./line-detail.component.scss']
})
export class LineDetailComponent implements OnInit, OnDestroy {
  state: any;
  lineDetailMeta = {
    category: '',
    data: {},
    clusters: {}
  }

  clusters: ClusterData[];
  route: Line;
  descriptions: PingUpdateDescription[] = [];

  constructor (private mapFiltersService: MapFiltersService, private routeService: RouteService, private router: Router, private pingClustersSource: PingClustersSource,
    private linesSource: MLayersLinesSource, private pingMapService: PingMapService, private lineService: LinesService, private apiMService: ApiMService) {
  }

  ngOnInit() {
    this.descriptions = this.routeService.currentRouteStaticData.data.description;

    const lineId = this.routeService.currentRouteStaticData.params.lineId;
    this.route = this.lineService.find(lineId);
    const mapLineId = `line:${this.route.type}:${lineId}`;

    this.mapFiltersService.setState([mapLineId]).subscribe(() => {
      const lineFeature: Feature<Geometry> = (this.linesSource.getSource() as VectorSource).getFeatureById(lineId);
      if (lineFeature) {
        const extent: Extent = lineFeature.getGeometry().getExtent();
        if (extent) {
          this.pingMapService.fitView(extent, 0);
        }
      }
    });

    this.apiMService.getClustersOfRoute(lineId).subscribe(data => {
      this.clusters = data;
      this.pingClustersSource.activateFeatures(this.clusters, this.route.textColor, this.route.color, false);
      this.pingMapService.showPoisLayer();
    })
  }


  ngOnDestroy() {
    this.pingMapService.hidePoisLayer();
    this.mapFiltersService.setState([]);
  }

}
