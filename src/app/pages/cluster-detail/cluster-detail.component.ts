import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteService } from 'src/app/services/route.service';
import { ApiMService } from 'src/app/services/api-m.service';
import { Poi } from '@metromobilite/m-features/reference';
import { ClusterData, PingMapService, RouteData } from '@m-ping';
import { MapFiltersService } from '@metromobilite/m-map/m-layers';
import { MatDividerModule } from '@angular/material/divider';
import { PingClustersSource } from '@m-ping/features/pois/map/source/pois.source';
import { PingUpdatesAccordionComponent } from 'src/app/components/ping-updates-accordion/ping-updates-accordion.component';
import { LinesService } from '@metromobilite/m-features/core';
import { fromLonLat } from 'ol/proj';
import { ClusterDecorationComponent } from "../../components/cluster-decoration/cluster-decoration.component";
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subject, takeUntil } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { GlobalService } from '@m-ping/services/global.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
    selector: 'app-cluster-detail',
    standalone: true,
    templateUrl: './cluster-detail.component.html',
    styleUrls: ['./cluster-detail.component.scss'],
    imports: [CommonModule, MatDividerModule, PingUpdatesAccordionComponent, ClusterDecorationComponent, MatIconModule, MatButtonModule]
})
export class ClusterDetailComponent implements OnInit, OnDestroy {
  isMobile = false;
  cluster: Poi;
  clusterRoutes: RouteData[];
  routeIds: string[] = [];
  private unSubscriber = new Subject<void>();


  constructor (public routeService: RouteService, private mapFiltersService: MapFiltersService, private pingClusterSource: PingClustersSource, private pingMapService: PingMapService, private lineService: LinesService, private breakpointObserver: BreakpointObserver, public globalService: GlobalService
  ) { }

  ngOnInit(): void {
    this.breakpointObserver.observe(['(max-width: 770px)']).pipe(takeUntil(this.unSubscriber)).subscribe((result) => {
			this.isMobile = result.matches;
		});
    this.cluster = this.routeService.currentRouteStaticData.data.cluster;
    this.clusterRoutes = this.routeService.currentRouteStaticData.data.clusterRoutes;
    this.routeIds = this.routeService.currentRouteStaticData.data.clusterRoutes.map(route => route.id);

    const states: string[] = [];
    this.clusterRoutes
      .map((route: RouteData) => {
        const agencyId = route.id.split(':')[0];
        if (['NEW'].includes(agencyId)) {
          states.push(`line:${route.type}:${route.id}`);
        }
        return route.id;
      });

    const oldLineColorConfig = {
      color: '95989A',
      textColor: '000'
    };
    const transformedCluster = this.transformPoiToClusterData(this.cluster);

    this.pingClusterSource.activateSearchedCluster(transformedCluster, oldLineColorConfig.textColor, oldLineColorConfig.color, true);
    this.pingMapService.showPoisLayer();
    this.pingMapService.setMapPosition({ zoom: 13, center: fromLonLat(this.cluster.geometry.coordinates) }).subscribe();
    this.mapFiltersService.setState(states).subscribe();
  }

  ngOnDestroy(): void {
    this.pingMapService.hidePoisLayer();
    this.mapFiltersService.setState([]);
  }


  transformPoiToClusterData(poi: Poi): ClusterData {
    if (!poi.properties.id || !poi.properties.code || !poi.properties.city || !poi.properties.name || poi.properties.visible === undefined) {
      throw new Error("Missing required properties in Poi object.");
    }
    return {
      id: poi.properties.id,
      code: poi.properties.code,
      city: poi.properties.city,
      name: poi.properties.name,
      visible: poi.properties.visible,
      lat: poi.geometry.coordinates[1],
      lon: poi.geometry.coordinates[0]
    };
  }
}
