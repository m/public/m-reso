import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { MPingMapComponent, PingMapService, PingService } from '@m-ping';
import { LogoMComponent } from "../../components/logo-m/logo-m.component";
import { RouteService } from 'src/app/services/route.service';
import { Subject, takeUntil } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { LogoLinesDisplayComponent } from 'src/app/components/logo-lines-display/logo-lines-display.component';
import { FindRouteDetailsPipe } from 'src/app/pipes/find-route-details.pipe';
import { FindPingUpdatesPipe } from "../../pipes/find-ping-updates.pipe";
import { MatButtonModule } from '@angular/material/button';
import { RemoveSPipe } from 'src/app/pipes/remove-s.pipe';
import { CurrentRouteStaticData } from 'src/app/models/reseau.model';
import { ThemeToggleComponent } from "../../components/theme-toggle/theme-toggle.component";
import { ThemeToggleService } from '@m-ping/services/theme-toggle.service';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BreakpointObserver } from '@angular/cdk/layout';
import { GlobalService } from '@m-ping/services/global.service';
import { CommuneZoneSource } from '@m-ping/features/communes-zones/map/source/communes-zones.source';
import VectorSource from 'ol/source/Vector';

@Component({
  selector: 'app-visualization',
  standalone: true,
  templateUrl: './visualization.component.html',
  styleUrls: ['./visualization.component.scss'],
  imports: [CommonModule, MPingMapComponent, LogoMComponent, RouterModule, MatIconModule, MatDividerModule,
    FindRouteDetailsPipe, LogoLinesDisplayComponent, FindPingUpdatesPipe, MatButtonModule, RemoveSPipe, ThemeToggleComponent, MatSidenavModule]
})
export class VisualizationComponent implements OnInit, OnDestroy {

  @ViewChild('routerOutletContainer') routerOutletContainer: ElementRef;
  @ViewChild('headerContainer') headerContainer: ElementRef;

  private unSubscriber = new Subject<void>();
  currentRouteStaticData: CurrentRouteStaticData;
  searchId: string;
  isOpen = true;

  constructor(public activatedRoute: ActivatedRoute, private pingService: PingService, public routeService: RouteService,
    private location: Location, public themeService: ThemeToggleService, private mPingMapService: PingMapService,
    public router: Router, public globalService: GlobalService, private communeZoneSource: CommuneZoneSource) { }

  ngOnInit(): void {
    this.routeService.currentRouteStaticData$.pipe(takeUntil(this.unSubscriber)).subscribe((currentRouteStaticData: CurrentRouteStaticData) => {
      this.currentRouteStaticData = currentRouteStaticData;
      this.searchId = currentRouteStaticData.params.oldLineId || currentRouteStaticData.params.newLineId || currentRouteStaticData.params.lineId;
      const code: string = this.currentRouteStaticData.queryParams['zone'];
      if (code) {
        this.communeZoneSource.getData(code).pipe(takeUntil(this.unSubscriber)).subscribe(result => {
          if (!!result) {
            this.mPingMapService.showPolygonLayer();
            this.mPingMapService.centerOnZone();
          }
        });
      } else if ((this.communeZoneSource.zoneSource as VectorSource) && (this.communeZoneSource.zoneSource as VectorSource).getFeatures().length > 0) {
        this.mPingMapService.centerOnZone();
      }
    });
  }

  ngOnDestroy(): void {
    this.pingService.clearView();
    this.unSubscriber.next();
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: Event): void {
    let element = this.routerOutletContainer?.nativeElement;
    const scrollPosition = element.scrollTop;
    if (scrollPosition > 10) {
      this.headerContainer?.nativeElement.classList.add('shadow-bottom-effect');
    } else {
      this.headerContainer?.nativeElement.classList.remove('shadow-bottom-effect');

    }
  }

  goBack() {
    this.location.back(); // uses browser history to go to the previous page
  }

  toggle() {
    if (!this.isOpen || !this.currentRouteStaticData.data.topLevel) {
      this.isOpen = true;
      this.router.navigate(['./visualisation'])
    } else if (this.isOpen || this.currentRouteStaticData.data.topLevel) {
      this.isOpen = false;
      this.goBack();
    }
  }
}
