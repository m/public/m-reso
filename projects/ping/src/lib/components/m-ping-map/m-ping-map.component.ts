import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayerInputConfig, MMapModule, MapComponent, MapLayersService } from '@metromobilite/m-map';
import { FormsModule } from '@angular/forms';
import { CoreModule, Line, LinesService, THEMES } from '@metromobilite/m-features/core';
import { PingMapService } from '../../services/ping-map.service';
import { MapFiltersService, MLayersLinesSource } from '@metromobilite/m-map/m-layers';
import { Subject, takeUntil } from 'rxjs';
import { PingService } from '@m-ping';
import { PingClustersSource } from '@m-ping/features/pois/map/source/pois.source';
import BaseEvent from 'ol/events/Event';
import { Feature, MapBrowserEvent, Overlay } from 'ol';
import { MatTooltipModule } from '@angular/material/tooltip';
import { Coordinate } from 'ol/coordinate';
import { Point, Polygon } from 'ol/geom';
import { PingLegendComponent } from "../../features/ping-legend/ping-legend.component";
import VectorLayer from 'ol/layer/Vector';

import Style from 'ol/style/Style';
import { ThemeToggleService } from '@m-ping/services/theme-toggle.service';
import { PingUpdatesAccordionComponent } from "../../../../../../src/app/components/ping-updates-accordion/ping-updates-accordion.component";
import { IncludesAnyPipe } from "../../pipes/includes-any.pipe";
import { LinePopupLabelPipe } from "../../pipes/line-popup-label.pipe";
import { CommuneZoneSource } from '@m-ping/features/communes-zones/map/source/communes-zones.source';
import VectorSource from 'ol/source/Vector';

@Component({
  selector: 'm-ping-map',
  standalone: true,
  templateUrl: './m-ping-map.component.html',
  styleUrls: ['./m-ping-map.component.scss'],
  imports: [CommonModule, MMapModule, FormsModule, MatTooltipModule, CoreModule, PingLegendComponent, PingUpdatesAccordionComponent, IncludesAnyPipe, LinePopupLabelPipe]
})
export class MPingMapComponent implements OnInit, AfterViewInit, OnDestroy {

  objectKeys = Object.keys;

  mapBehaviors = ['core:init', 'core:map-events', 'core:after-init', 'core:animated-zoom'];
  tiles: LayerInputConfig[] = [
    { layer: 'core:xyz-light', visible: false },
    { layer: 'core:xyz-dark', visible: true },
  ];
  activeFeatures: { [key: string]: any[] };
  layers: LayerInputConfig[] = [];
  zoom = 12;
  displayCenter = false;
  private unSubscriber = new Subject<void>();
  private poisLayer: LayerInputConfig;
  private polygonsLayer: LayerInputConfig;
  private pemsLayer: LayerInputConfig;
  private lineDiffLayer: LayerInputConfig;
  private beforeLayer: LayerInputConfig;
  private afterLayer: LayerInputConfig;
  popupVisible = false;
  popupContent = '';
  private clusterPopupOverlay: Overlay;
  private linePopupOverlay: Overlay;
  private zonePopupOverlay: Overlay;
  linePopupContent: { [key: string]: Line[] } = {};
  zonePopupContent: Line[] = [];

  lineDetailsVisible = false;
  selectedRouteIds: string[] = [];
  polygonFeature: Feature<Polygon> = new Feature<Polygon>();
  enableLegend: boolean = false;

  set theme(value: THEMES) {
    this.tiles.forEach((tile) => {
      switch (tile.layer) {
        case 'core:xyz-light':
          tile.visible = value === THEMES.LIGHT;
          break;
        case 'core:xyz-dark':
        default:
          tile.visible = value === THEMES.DARK;
          break;
      }
    });
    this.tiles = JSON.parse(JSON.stringify(this.tiles));
    this.communeZoneSource.changed();
    this.pingClustersSource.changed();
    this.mLayerslineSource.changed();
    this.updateActiveFeatures();
    this.pingMapService.updateLayerConfigs();
  }

  @ViewChild('map', { static: true }) mapComponent: MapComponent;
  @ViewChild('clusterPopup', { static: true }) clusterPopupRef: ElementRef;
  @ViewChild('linePopup', { static: true }) linePopupRef: ElementRef;
  @ViewChild('zonePopup', { static: true }) zonePopupRef: ElementRef;
  @ViewChild('mPingFeaturesHost', { read: ViewContainerRef, static: true }) mPingFeaturesHost!: ViewContainerRef;


  constructor(private pingMapService: PingMapService, private mapFiltersService: MapFiltersService, private cdr: ChangeDetectorRef,
    private layersService: MapLayersService, public pingService: PingService, private pingClustersSource: PingClustersSource,
    private mLayerslineSource: MLayersLinesSource, private lineService: LinesService, private themeService: ThemeToggleService, private communeZoneSource: CommuneZoneSource) {
    if (localStorage.getItem(ThemeToggleService.LS_KEY) !== null) {
      this.theme = localStorage.getItem(ThemeToggleService.LS_KEY) as THEMES;
    }
  }

  ngOnInit(): void {
    this.initPingMap();
    this.pingService.setViewContainerRef(this.mPingFeaturesHost);
    this.pingMapService.featureSelected$.pipe(takeUntil(this.unSubscriber)).subscribe(feature => {
      if (feature) {
        const geometry = feature.getGeometry() as Point;
        if (feature.getGeometry().getType() === 'Point') {
          this.showClusterName(geometry.getCoordinates(), feature.get('name'));
        }
      } else {
        this.hidePopup();
      }
    });
  }

  ngAfterViewInit(): void {
    this.initPopupOverlay();
    this.initLinePopupOverlay();
  }

  ngOnDestroy(): void {
    this.unSubscriber.next();
    this.unSubscriber.complete();
  }

  initPingMap() {
    this.pingMapService.setMap(this);
    this.mapFiltersService
      .init(this.mapComponent)
      .pipe(takeUntil(this.unSubscriber))
      .subscribe(() => {
        if (this.mapFiltersService.initialized) {
          this.lineDiffLayer = { layer: 'm-ping:line-diff-visualizer', visible: false };
          this.poisLayer = { layer: 'm-ping:pois', visible: false };
          this.polygonsLayer = { layer: 'm-ping:commune-zone', visible: true };
          this.pemsLayer = { layer: 'm-ping:pems', visible: false };
          this.beforeLayer = { layer: 'm-ping:before', visible: false };
          this.afterLayer = { layer: 'm-ping:after', visible: false };
          const mapFiltersLineLayers = this.mapFiltersService.layers.filter(layerConfig => layerConfig.layer === 'm-layers:line');
          this.layers = [...mapFiltersLineLayers, this.polygonsLayer, this.lineDiffLayer, this.beforeLayer, this.afterLayer, this.poisLayer];
          this.pingMapService.updateLayerConfigs();
          this.updateActiveFeatures();
        }
        // Waiting for layers init.
        setTimeout(() => {
          this.pingMapService.initMap.next(this.mapFiltersService.initialized);
        });
      });
  }

  private initPopupOverlay(): void {
    this.clusterPopupOverlay = new Overlay({
      element: this.clusterPopupRef.nativeElement,
      stopEvent: false,
      offset: [10, 0],
      autoPan: true
    });
    this.mapComponent.map.addOverlay(this.clusterPopupOverlay);
  }

  private initLinePopupOverlay(): void {
    this.linePopupOverlay = new Overlay({
      element: this.linePopupRef.nativeElement,
      stopEvent: false,
      offset: [10, 0],
      autoPan: true
    });
    this.mapComponent.map.addOverlay(this.linePopupOverlay);
  }


  onMapClick(event: BaseEvent): void {
    if (this.pingService.selectedFeature) {
      this.pingService.clearSelectedFeature();
      this.hidePopup();
    }
    if (event instanceof MapBrowserEvent) {
      const clickedLineIds: string[] = [];
      
      event.map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
        if (!this.pingService.selectedFeature) {
          const geometry = feature.getGeometry() as Point;
          const type = geometry.getType();
          if (type === 'Point') {
            this.polygonFeature = null;
            this.pingService.selectedFeature = feature as Feature;
            this.pingService.selectedFeature.set('selected', true);
            this.showClusterName(geometry.getCoordinates(), feature.get('name'));
            this.pingMapService.updateSelectedFeature(this.pingService.selectedFeature);
          } else if (type === 'LineString' || type === 'MultiLineString') {
            clickedLineIds.push(feature.getId() as string);
          } else if (type === 'Polygon' || type === 'MultiPolygon') {
            this.polygonFeature = feature as Feature<Polygon>;
          }
        }
      });
      if (clickedLineIds.length > 0) {
        const coordinate = event.coordinate;
        this.showLinePopup(coordinate, clickedLineIds);
      } else {
        if (this.polygonFeature) {
          this.handlePolygonClick(this.polygonFeature as Feature<Polygon>);
        }
        this.hideLinePopup();
      }
    }
  }

  handlePolygonClick(polygonFeature: Feature<Polygon>): void {
    this.zonePopupContent = [];
    const polygonGeometry = polygonFeature.getGeometry();
    if (polygonGeometry) {
      this.layers.forEach(layerConfig => {
        const layer = this.mapComponent.map.getAllLayers().find(layer => {
          const layerId = layer.getProperties()['m-map:layer:id'];
          return layerId === layerConfig.layer;
        });
        if (layer instanceof VectorLayer) {
          (layer.getSource() as VectorSource).forEachFeature((feature) => {
            if (feature.getGeometry().intersectsExtent(polygonGeometry.getExtent()) && feature.getId() !== polygonFeature.getId()) {
              // console.log('Intersecting feature found:', feature.getId());
              const line = this.lineService.linesAsMap[feature.getId()];
              this.zonePopupContent.push(line);
            }
          });
        }
      });
    }
  }

  showLinePopup(coordinate: Coordinate, ids: string[]): void {
    this.linePopupContent = {};

    ids.forEach(id => {
      const line = this.lineService.linesAsMap[id];
      const agencyId = id.split(':')[0];
      if (!this.linePopupContent[agencyId]) {
        this.linePopupContent[agencyId] = [];
      }
      this.linePopupContent[agencyId].push(line);
    });

    this.linePopupOverlay.setPosition(coordinate);
  }

  updateActiveFeatures() {
    this.activeFeatures = null;
    if (this.mapComponent) {
      const activeLayers = this.mapComponent.map.getAllLayers().filter(layer => {
        const layerId = layer.getProperties()['m-map:layer:id'];
        return layer.getVisible() && !['core:xyz-light', 'core:xyz-dark', 'm-ping:before', 'm-ping:after', 'm-ping:pois'].includes(layerId);
      });

      if (activeLayers && activeLayers.length) {
        this.activeFeatures = {};

        activeLayers.forEach(layer => {
          if (layer instanceof VectorLayer) {
            const layerId = layer.getProperties()['m-map:layer:id'];

            if (!this.activeFeatures[layerId]) this.activeFeatures[layerId] = [];

            const features = layer.getSource().getFeatures();
            features.forEach(feature => {

              const styles = this.extractFeatureStyles(layer, feature);
              if (styles.length > 0) { // Only add features with non-empty styles
                this.activeFeatures[layerId].push({
                  properties: { id: feature.getId(), ...feature.getProperties() },
                  styles: styles
                });
              }
            });
          }
        });
      }
    }
  }

  private extractFeatureStyles(layer: VectorLayer<any>, feature: Feature): any[] {
    let styles = [];

    const styleFunction = layer.getStyleFunction();
    let style = styleFunction ? styleFunction(feature, 1) : [];

    if (style) {
      if (Array.isArray(style)) {
        styles = style.flatMap((s: Style) => {
          return this.getConfigFromStyle(s);
        }).filter(styleConfig => this.isStyleValid(styleConfig));
      } else {
        style = style as Style;
        const styleConfig = this.getConfigFromStyle(style);
        if (this.isStyleValid(styleConfig)) {
          styles.push(styleConfig);
        }
      }
    }

    return styles;
  }

  private getConfigFromStyle(style: Style): any {
    const fillColor = style.getFill() ? style.getFill().getColor() : null;
    const strokeColor = style.getStroke() ? style.getStroke().getColor() : null;
    const strokeWidth = style.getStroke() ? style.getStroke().getWidth() : null;
    return {
      fillColor,
      strokeColor,
      strokeWidth
    };
  }

  private isStyleValid(styleConfig: any): boolean {
    return styleConfig.fillColor !== null || styleConfig.strokeColor !== null || styleConfig.strokeWidth !== null;
  }

  private showClusterName(coordinate: Coordinate, content: string): void {
    this.clusterPopupOverlay.setPosition(coordinate);
    this.popupContent = content;
    this.clusterPopupOverlay.getElement().style.display = 'flex';
  }

  hidePopup(): void {
    this.clusterPopupOverlay.setPosition(undefined);
    this.linePopupOverlay.setPosition(undefined);
  }

  hideLinePopup(): void {
    this.linePopupOverlay.setPosition(undefined);
  }

  showLineLayers(): void {
    if (this.lineDiffLayer && this.lineDiffLayer._uid) {
      this.layersService.show(this.mapComponent, this.lineDiffLayer._uid);
    }
  }

  hideLineLayers(): void {
    if (this.lineDiffLayer && this.lineDiffLayer._uid) {
      this.layersService.hide(this.mapComponent, this.lineDiffLayer._uid);
    }
    this.hidePopup();
  }

  showPolygonLayer(): void {
    if (this.polygonsLayer && this.polygonsLayer._uid) {
      this.layersService.show(this.mapComponent, this.polygonsLayer._uid);
    }
  }

  hidePolygonLayer(): void {
    if (this.polygonsLayer && this.polygonsLayer._uid) {
      this.layersService.hide(this.mapComponent, this.polygonsLayer._uid);
    }
  }

  showBeforeLayer(): void {
    if (this.beforeLayer && this.beforeLayer._uid) {
      this.layersService.show(this.mapComponent, this.beforeLayer._uid);
    }
  }

  hideBeforeLayer(): void {
    if (this.beforeLayer && this.beforeLayer._uid) {
      this.layersService.hide(this.mapComponent, this.beforeLayer._uid);
    }
    this.hidePopup();
  }

  showAfterLayer(): void {
    if (this.afterLayer && this.afterLayer._uid) {
      this.layersService.show(this.mapComponent, this.afterLayer._uid);
    }
  }

  hideAfterLayer(): void {
    if (this.afterLayer && this.afterLayer._uid) {
      this.layersService.hide(this.mapComponent, this.afterLayer._uid);
    }
    this.hidePopup();
  }

  showBeforeAfterLayers(): void {
    this.showBeforeLayer();
    this.showAfterLayer();
    this.enableLegend = false;
    this.cdr.detectChanges();
  }

  hideBeforeAfterLayers(): void {
    this.hideBeforeLayer();
    this.hideAfterLayer();
    this.enableLegend = true;
    this.cdr.detectChanges();
  }

  hidePoisLayer(): void {
    if (this.poisLayer && this.poisLayer._uid) {
      this.pingClustersSource.setFeaturesVisibility(false);
      this.layersService.hide(this.mapComponent, this.poisLayer._uid);
    }
    this.hidePopup();
  }

  showpoisLayer(): void {
    if (this.poisLayer && this.poisLayer._uid) {
      this.layersService.show(this.mapComponent, this.poisLayer._uid);
    }
  }

  hidePemsLayer(): void {
    if (this.pemsLayer && this.pemsLayer._uid) {
      this.layersService.hide(this.mapComponent, this.pemsLayer._uid);
    }
    this.hidePopup();
  }

  showPemsLayer(): void {
    if (this.pemsLayer && this.pemsLayer._uid) {
      this.layersService.show(this.mapComponent, this.pemsLayer._uid);
    }
  }

  showStaticLayers(): void {
    this.showLineLayers();
    this.showpoisLayer();
  }

  hideStaticLayers(): void {
    this.hideLineLayers();
    this.hidePoisLayer();
    this.hidePemsLayer();
  }
}
