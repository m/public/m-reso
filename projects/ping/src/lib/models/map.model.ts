import { MapComponent } from '@metromobilite/m-map';
import { Feature, MapBrowserEvent } from 'ol';
import { Coordinate } from 'ol/coordinate';
import { Layer } from 'ol/layer';
import { Subject } from 'rxjs';

export interface GeolocationBehaviorRef {
  map: MapComponent;
  unSubscriber: Subject<void>;
  lastPosition?: Coordinate;
  skipFirst?: boolean;
}

export class MapEvent {
  constructor (public originalEvent: MapBrowserEvent<any>) { }
}

export class MapFeatureClickedEvent extends MapEvent {
  constructor (
    public feature: Feature,
    public layer: Layer,
    public override originalEvent: MapBrowserEvent<any>
  ) { super(originalEvent); }
}


export interface ParamsMap {
  zoom?: number;
  center?: number[];
  duration?: number;
}

export interface FeatureCollection {
    type: string;
    features: Feature[];
}