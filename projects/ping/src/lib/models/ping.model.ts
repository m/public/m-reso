export interface PingFeature {
  pingFeatureId: string;
  configuration?: PingFeatureConfiguration;
}

export interface PingFeatureConfiguration {
  [key: string]: any;
}



export interface LayerComparison {
  [key: string]: LayerDetails;
}

export interface LayerDetails {
  agencies: AgencyConfig[];
  types: LayerTypeConfig[];
  routes: RouteConfig[];
}

export interface AgencyConfig {
  agencyIds: string[];
  zIndex: number;
}

export interface LayerTypeConfig {
  typeIds: string[];
  zIndex: number;
}

export interface RouteConfig {
  routeIds: string[];
  zIndex: number;
}

export interface Updates {
  nouvellesLignes: UpdateCategory;
  lignesModifieesTrace: UpdateCategoryWithLineDiff;
  lignesModifieesNom: UpdateCategoryWithDisplayOldLine;
  lignesModifieesTraceEtNom: UpdateCategoryWithDisplayOldLineAndLineDiff;
  lignesModifieesHoraires: UpdateCategory;
}

export interface UpdateCategory {
  category: string;
  order: number;
  data: UpdateData[];
}

export interface UpdateCategoryWithDisplayOldLine extends UpdateCategory {
  displayOldLine: boolean;
}

export interface UpdateCategoryWithLineDiff extends UpdateCategory {
  lineDiff: boolean;
}

export interface UpdateCategoryWithDisplayOldLineAndLineDiff extends UpdateCategoryWithDisplayOldLine, UpdateCategoryWithLineDiff { }

export interface UpdateData {
  id: string;
  description: DescriptionDetail[];
  oldId?: string;
}

export interface DescriptionDetail {
  label: string;
  content: string;
}

export interface PingUpdates {
  layerComparison: LayerComparison;
  updates: Updates;
}

export enum ThemeEnum {
  LIGHT = 'light-theme',
  DARK = 'dark-theme'
}


export enum LineAgencyLabelsEnum {
  OLD = 'Ancien réseau',
  NEW = 'M réso'
}

export interface Commune {
  gid: number
  ref_insee: string
  commune: string
  wikipedia: string
  surf_ha: number
  code: string
  epci: string
  bassin_air: any
  rectangle: boolean
}

export interface MetaLabels {
  NEW: string,
  OLD: string
}