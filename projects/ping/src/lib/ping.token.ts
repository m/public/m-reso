import { InjectionToken } from '@angular/core';
import { PingFeature } from './models/ping.model';
import { PingLegendHandler } from './features/ping-legend/core/legend-handler.interface';

export const PING_FEATURES = new InjectionToken<PingFeature>('PING_FEATURES');
export const PING_LEGEND_HANDLERS = new InjectionToken<PingLegendHandler>('PING_LEGEND_HANDLERS');
