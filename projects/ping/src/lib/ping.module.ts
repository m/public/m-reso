import { APP_INITIALIZER, NgModule } from '@angular/core';
import { PING_FEATURES, PING_LEGEND_HANDLERS } from './ping.token';
import { BeforeAfterMapViewerComponent, LineDiffVizualizerComponent } from './features';
import { MAP_LAYER } from '@metromobilite/m-map';
import { PingClustersLayer } from './features/pois/map/layers/pois.layer';
import { LineDiffVisualizerLayer } from './features/line-diff-visualizer/map/layers/line-diff-visualizer.layer';
import { PingBeforeLayer } from './features/before-after-map-viewer/map/layers/before.layer';
import { PingAfterLayer } from './features/before-after-map-viewer/map/layers/after.layer';
import { ConfigService, FRONT_CONFIG } from '@metromobilite/m-features/core';
import { PemsLayer } from './features/pois/map/layers/pem.layer';
import { ThemeToggleComponent } from '../../../../src/app/components/theme-toggle/theme-toggle.component';
import { LineDiffPingLengendHandler } from './features/ping-legend/core/handlers/line-diff.legend.handler';
import { DefaultPingLengendHandler } from './features/ping-legend/core/handlers/default.legend.handler';
import { LineDetailLengendHandler } from './features/ping-legend/core/handlers/line-detail.legend.handler';
import { CommuneZoneLayer } from './features/communes-zones/map/layers/communes-zones.layer';
import { CommuneZoneLengendHandler } from './features/ping-legend/core/handlers/commune-zone.legend.handler';


export function initializeFrontConfigService(configService: ConfigService) {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      console.info('initializeFrontConfigService - started');
      configService.getData(FRONT_CONFIG.PING_RESEAU_UPDATES).subscribe({
        next: (config) => {
          console.info('initializeFrontConfigService - completed');
          resolve(config);
        },
        error: (err) => reject(err)
      });
    });
  };
}


@NgModule({
  declarations: [],
  imports: [
  ],
  exports: [],
  providers: [
    { provide: APP_INITIALIZER, useFactory: initializeFrontConfigService, deps: [ConfigService], multi: true },
    { provide: PING_FEATURES, useClass: BeforeAfterMapViewerComponent, multi: true },
    { provide: PING_FEATURES, useClass: LineDiffVizualizerComponent, multi: true },
    { provide: PING_FEATURES, useClass: ThemeToggleComponent, multi: true },
    { provide: MAP_LAYER, useClass: LineDiffVisualizerLayer, multi: true },
    { provide: MAP_LAYER, useClass: PingClustersLayer, multi: true },
    { provide: MAP_LAYER, useClass: PemsLayer, multi: true },
    { provide: MAP_LAYER, useClass: PingBeforeLayer, multi: true },
    { provide: MAP_LAYER, useClass: PingAfterLayer, multi: true },
    { provide: MAP_LAYER, useClass: CommuneZoneLayer, multi: true },
    { provide: PING_LEGEND_HANDLERS, useClass: DefaultPingLengendHandler, multi: true },
    { provide: PING_LEGEND_HANDLERS, useClass: LineDiffPingLengendHandler, multi: true },
    { provide: PING_LEGEND_HANDLERS, useClass: LineDetailLengendHandler, multi: true },
    { provide: PING_LEGEND_HANDLERS, useClass: CommuneZoneLengendHandler, multi: true }
  ],
})
export class PingModule { }
