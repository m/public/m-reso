import { BreakpointObserver } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private unSubscriber = new Subject<void>();
  isHigh = true;
  isMobile = false;


  constructor(private breakpointObserver: BreakpointObserver) {
    this.breakpointObserver.observe(['(max-width: 770px)']).pipe(takeUntil(this.unSubscriber)).subscribe((result) => {
			this.isMobile = result.matches;
		});
  }

  goDown() {
    this.isHigh = !this.isHigh;
  }
}
