import { Inject, Injectable, Type, ViewContainerRef } from '@angular/core';
import { PingFeature } from '../models/ping.model';
import { PING_FEATURES, PING_LEGEND_HANDLERS } from '../ping.token';
import { MapFiltersService } from '@metromobilite/m-map/m-layers';
import { PingMapService } from './ping-map.service';
import { Feature } from 'ol';
import { PingLegendHandler } from '@m-ping/features/ping-legend/core/legend-handler.interface';

@Injectable({
  providedIn: 'root'
})
export class PingService {

  private pingFeaturesMap: { [key: string]: Type<PingFeature> } = {};
  //TODO bring this to MapService
  public selectedFeature: any;
  viewContainerRef: ViewContainerRef;

  constructor(@Inject(PING_FEATURES) components: PingFeature[], @Inject(PING_LEGEND_HANDLERS) private legendHandlers: PingLegendHandler[], private mapFiltersService: MapFiltersService, private pingMapService: PingMapService) {
    components.forEach(component => {
      this.pingFeaturesMap[component.pingFeatureId] = component.constructor as Type<PingFeature>;
    });
  }

  getComponent(type: string): Type<PingFeature> {
    let component = this.pingFeaturesMap[type];
    if (!component) {
      const validTypes = Object.keys(this.pingFeaturesMap).join(', ');
      const errorMessage = `Component of type: '${type}' does not exist. Make sure to do any of the following:
      - Use existing types: ${validTypes}
      - If '${type}' is your own component: register it with the 'PING_FEATURES' token in the 'providers' section of the correct module in your app.
      `
      throw new Error(errorMessage.trim());
    }
    return component;
  }

  getLegendHandler(layerId: string): PingLegendHandler {
    return this.legendHandlers.find(handler => handler.handlerIds.includes(layerId));
  }


  activateFeature(config: PingFeature) {
    this.viewContainerRef.clear();
    const componentRef = this.viewContainerRef.createComponent<PingFeature>(this.getComponent(config.pingFeatureId));
    componentRef.instance.configuration = config.configuration;
  }

  setViewContainerRef(viewContainerRef: ViewContainerRef) {
    this.viewContainerRef = viewContainerRef;
  }

  clearView(): void {
    this.mapFiltersService.setState([]);
    this.viewContainerRef.clear();
    this.pingMapService.hideLayers();
    this.pingMapService.hidePopUpOverlay();
    if (this.selectedFeature) this.selectedFeature.set('selected', false);
  }

  clearSelectedFeature(): void {
    this.selectedFeature.set('selected', false);
    this.selectedFeature = undefined;
  }

}
