import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { PingMapService } from '@m-ping';
import { PingClustersSource } from '@m-ping/features/pois/map/source/pois.source';
import { THEMES } from '@metromobilite/m-features/core';
import { LayerInputConfig } from '@metromobilite/m-map';
import { Geometry } from 'ol/geom';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';

@Injectable({
  providedIn: 'root'
})
export class ThemeToggleService {
  strokeColorCluster: string;
  fillColorCluster: string;
  public static readonly LS_KEY: string = 'm-ping:theme';

  theme: THEMES;
  constructor(@Inject(DOCUMENT) private _document: any, private pingMapService: PingMapService, private pingClustersSource: PingClustersSource) {
    let _theme: string = THEMES.LIGHT;
    try {
      _theme = localStorage.getItem(ThemeToggleService.LS_KEY);
      if (localStorage.getItem(ThemeToggleService.LS_KEY) === null) {
        localStorage.setItem(ThemeToggleService.LS_KEY, THEMES.DARK);
        _theme = localStorage.getItem(ThemeToggleService.LS_KEY);
      }
      this.theme = _theme as THEMES;
      this.setClasses();
    } catch (e) {
      console.log(e)
    }
  }

  public toggleTheme(theme: THEMES): void {
    this.theme = theme;
    this.setStorage();
    this.setClasses();
    this.setMapTiles();
  }

  public setClasses(): void {
    if (this.theme === THEMES.LIGHT) {
      this._document.body.classList.remove(THEMES.DARK);
      this._document.body.classList.add(THEMES.LIGHT);
      this.strokeColorCluster = '#000000';
      this.fillColorCluster = "#FFFFFF";
    } else if (this.theme === THEMES.DARK) {
      this._document.body.classList.remove(THEMES.LIGHT);
      this._document.body.classList.add(THEMES.DARK);
      this.strokeColorCluster = "#FFFFFF";
      this.fillColorCluster = '#000000';
    }
  }

  private setStorage(): void {
    localStorage.setItem(ThemeToggleService.LS_KEY, this.theme);
  }

  private setMapTiles() {
    this.pingMapService.mPingMapComponent.theme = this.theme;
  }

}
