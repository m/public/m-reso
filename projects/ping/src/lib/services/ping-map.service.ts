import { Inject, Injectable } from '@angular/core';
import { MapFiltersService, MLayersLinesSource, } from '@metromobilite/m-map/m-layers';
import Feature from 'ol/Feature';
import { fromLonLat, transform } from 'ol/proj';
import { ConnectableObservable, Observable, Subject, Subscriber, publish, shareReplay } from 'rxjs';
import { Coordinate } from 'ol/coordinate';
import { LayerInputConfig, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapComponent } from '@metromobilite/m-map';
import { THEMES } from '@metromobilite/m-features/core';
import { MPingMapComponent } from '../components/m-ping-map/m-ping-map.component';
import Map from 'ol/Map';
import { Extent } from 'ol/extent';
import { ParamsMap } from '@m-ping';
import { CommuneZoneSource } from '@m-ping/features/communes-zones/map/source/communes-zones.source';

@Injectable({
	providedIn: 'root',
})
export class PingMapService {
	readonly HOME_MAP_STATE_KEY = '/accueil/bus-tram';
	initialized = false;
	theme: THEMES;
	initMap = new Subject<boolean>();
	mPingMapComponent: MPingMapComponent;
	selectedFeature?: Feature;
	userMapStates: string[];
  userLastMode: string;

  private layerClusters: LayerInputConfig;


  private featureSelectedSource = new Subject<Feature | null>();
  featureSelected$ = this.featureSelectedSource.asObservable();


	constructor(
		@Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
		@Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
		private linesSource: MLayersLinesSource,
    private mapFiltersService: MapFiltersService,
    private communeZoneSource: CommuneZoneSource
	) {
		this.initMap.subscribe((initialized) => {
			this.initialized = initialized;
		});

    //TODO: Handle themes' observable
		// this.theme = this.userSettings.theme;

	}


	/**
	 * Should be used by the map-wrapper only.
	 */
  setMap(mPingMapComponent: MPingMapComponent): void {
		this.mPingMapComponent = mPingMapComponent;
	}

  getMap(): Map {
    return this.mPingMapComponent.mapComponent.map;
  }

  getMMapComponent(): MapComponent {
    return this.mPingMapComponent.mapComponent;
  }

	selectFeature(feature: Feature | null): void {
		if (this.selectedFeature) {
			this.selectedFeature.set('selected', false);
		}
		this.selectedFeature = feature;
		if (feature) {
			feature.set('selected', true);
		}
	}

	transformCoordinates(coordinates: Coordinate, reverse = false, source = this.featureProjection, dataProjection = this.dataProjection): Coordinate {
		if (reverse) {
			return transform(coordinates, dataProjection, source);
		}
		return transform(coordinates, source, dataProjection);
	}

	updateLayerConfigs(): void {
		if (!this.mPingMapComponent) {
			return;
		}
		const useDark = this.mPingMapComponent.tiles.filter((config) => config.visible).reduce((_, config) => config.layer.includes('dark'), true);
		let needChangedCall = false;
    this.mPingMapComponent.layers.forEach((config: LayerInputConfig) => {
			if (['m-layers:line', 'm-layers:road-section', 'm-layers:cycle-path'].includes(config.layer)) {
				const oldValue = config['backgroundColor'];
				config['backgroundColor'] = !useDark ? '#fff' : '#000';
				if (oldValue !== config['backgroundColor']) {
					needChangedCall = true;
				}
			}
			return config;
		});
		if (needChangedCall) {
			this.linesSource.changed();
		}
	}

  showLineLayers(): void {
    this.mPingMapComponent.showLineLayers();
  }

  showPoisLayer(): void {
    this.mPingMapComponent.showpoisLayer();
  }

  hidePoisLayer(): void {
    this.mPingMapComponent.hidePoisLayer();
  }

  showPolygonLayer(): void {
    this.mPingMapComponent.showPolygonLayer();
  }

  hidePolygonLayer(): void {
    this.mPingMapComponent.hidePolygonLayer();
  }


  showPemsLayer(): void {
    this.mPingMapComponent.showPemsLayer();
  }

  hidePemsLayer(): void {
    this.mPingMapComponent.hidePemsLayer();
  }

  showBeforeLayer(): void {
    this.mPingMapComponent.showBeforeLayer();
  }

  hideBeforeLayer(): void {
    this.mPingMapComponent.hideBeforeLayer();
  }

  showAfterLayer(): void {
    this.mPingMapComponent.showAfterLayer();
  }

  hideAfterLayer(): void {
    this.mPingMapComponent.hideAfterLayer();
  }

  showBeforeAfterLayers(): void {
    this.mPingMapComponent.showBeforeAfterLayers();
  }
  hideBeforeAfterLayers(): void {
    this.mPingMapComponent.hideBeforeAfterLayers();
  }


  hideLayers(): void {
    this.mPingMapComponent.hideStaticLayers();
  }

  hidePopUpOverlay(): void {
    this.mPingMapComponent.hidePopup();
  }

  fitView(extent: Extent, offsetBottom = 0, duration = 350) {
    const padding = 32;
    const fit$ = new Observable((obs) => {
      this.getMMapComponent().view.fit(extent, {
        padding: [padding + 16, padding, padding + offsetBottom, padding],
        duration,
        callback: (complete) => {
          obs.next(complete);
          obs.complete();
        },
      });
    }).pipe(shareReplay(1));
    publish()(fit$).connect();
    return fit$;
  }

  updateSelectedFeature(feature: Feature) {
    this.featureSelectedSource.next(feature);
    this.setMapPosition({ center: feature.get('geometry').flatCoordinates }).subscribe();
  }


  setMapPosition(data: ParamsMap): Observable<boolean> {

    const position$: Observable<boolean> = new Observable<boolean>((obs: Subscriber<boolean>): void => {
      this.getMMapComponent().view.animate({
        center: data.center || null,
        zoom: data.zoom || this.getMMapComponent().view.getZoom(),
        duration: data.duration || 100
      }, (complete: boolean): void => {
        obs.next(complete);
        obs.complete();
      });
    }).pipe(shareReplay(1));
    const published: ConnectableObservable<any> = publish()(position$);
    published.connect();
    return position$;
  }

  centerToDefault() {
    this.setMapPosition({ zoom: 12, center: fromLonLat(this.mPingMapComponent.mapComponent.coordinates), duration: 150})
  }

  centerOnZone() {
    const extent = this.communeZoneSource.getExtent();
    this.fitView(extent, 48, 100);
  }
}
