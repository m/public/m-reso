import { Pipe, PipeTransform } from '@angular/core';
import { LineAgencyLabelsEnum } from '@m-ping';
import { ConfigService, FRONT_CONFIG } from '@metromobilite/m-features/core';

@Pipe({
  name: 'linePopupLabel',
  standalone: true
})
export class LinePopupLabelPipe implements PipeTransform {

  constructor(private configService: ConfigService) {}

  transform(value: string): string {   
    return this.configService.config[FRONT_CONFIG.PING_RESEAU_UPDATES].meta.labels[value] || value;
  }i

}
