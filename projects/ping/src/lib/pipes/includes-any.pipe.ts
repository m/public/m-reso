import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'includesAny',
  standalone: true
})
export class IncludesAnyPipe implements PipeTransform {

  transform(value: string, conditions: string[]): boolean {
    return conditions.some(condition => value.includes(condition));
  }

}
