import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayerBase, MapComponent, MapLayer } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { LineStyleConfig } from '@metromobilite/m-map/m-layers';
import { hexToRGBA } from '@m-ping';
import { MPingBeforeAfterLinesSource } from '../source/before-after.source';
import { LineDiffVisualizerStyle } from '@m-ping/features/line-diff-visualizer/map/style/line-diff-visualizer.style';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:after')
export class PingAfterLayer extends MapLayerBase {

  constructor (
    public override source: MPingBeforeAfterLinesSource,
    private lineStyle: LineDiffVisualizerStyle
  ) {
    super();
  }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        const layerType = feature.get('layerType');
        const zIndex = feature.get('zIndex') || 1;

        if (layerType !== 'after') {
          return [];
        }

        const lineStyleConfig: LineStyleConfig = {
          color: feature.get('color'),
          backgroundColor: config['backgroundColor'] || '#000',
          noBorder: true,
          width: 4,
          zOffset: zIndex
        };

        return this.lineStyle.build(lineStyleConfig);
      }
    });
    return layer;
  }
}
