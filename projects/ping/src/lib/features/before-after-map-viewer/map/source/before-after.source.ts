import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ConfigService, FRONT_CONFIG, Line, LinesService } from '@metromobilite/m-features/core';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import Polyline from 'ol/format/Polyline';
import LineString from 'ol/geom/LineString';
import MultiLineString from 'ol/geom/MultiLineString';
import SimpleGeometry from 'ol/geom/SimpleGeometry';
import VectorSource from 'ol/source/Vector';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { M_LAYERS_AVAILABLE_LINE_TYPES } from '@metromobilite/m-map/m-layers';
import { LayerComparison, RouteData, RouteShapeResponse } from '@m-ping';
import { Extent, createEmpty, extend } from 'ol/extent';
import { EFFECTIVE_AGENCY_IDS } from 'src/app/utils/constants';

@Injectable({ providedIn: 'root' })
export class MPingBeforeAfterLinesSource extends MapSourceBase {

  override source: VectorSource;
  private polyline = new Polyline();

  constructor (
    private http: HttpClient,
    private linesService: LinesService,
    private frontConfigService: ConfigService,
    @Inject(M_LAYERS_AVAILABLE_LINE_TYPES) public availableLineTypes: string[],
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string
  ) {
    super();
    this.source = new VectorSource({});
  }

  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  getExtent(): Extent {
    const features = this.source.getFeatures();

    let extent = createEmpty();

    features.forEach(feature => {
      const geometry = feature.getGeometry();
      const layerType = feature.get('layerType');
      if (geometry && layerType && (layerType === 'before' || layerType === 'after')) {
        extend(extent, geometry.getExtent());
      }
    })
    return extent;
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  public addLinesToSource(): Observable<boolean> {
    const config = this.frontConfigService.config[FRONT_CONFIG.PING_RESEAU_UPDATES].layerComparison;
    const excludedTypes = this.frontConfigService.config[FRONT_CONFIG.PING_RESEAU_UPDATES].meta.excludedTypes;
    
    return this.getAllRoutesShapes().pipe(
      map((response: RouteShapeResponse) => {
        const features = response.features;
        for (const feature of features) {
          const geometry = this.getGeometry(feature.properties.shape);
          const line = this.linesService.linesAsMap[feature.properties.id.replace('_', ':')];

          if (!geometry || !line || excludedTypes.includes(line.type)) continue;
          const { layerType, zIndex } = this.getLineLayerConfig(config, line);

          if (layerType != 'unknown') {
            const f = new Feature({
              geometry,
              type: line.type,
              color: `#${line.color}`,
              textColor: `#${line.textColor}`,
              layerType,
              zIndex
            });
            f.setId(line.id);
            this.push(f);
          }

        }

        return true;
      }),
      catchError(err => of(false))
    );
  }

  private getLineLayerConfig(config: LayerComparison, route: Line): { layerType: string; zIndex: number; } {
    const agencyId = route.id.split(':')[0];

    for (const key in config) {

      const routeConfig = config[key].routes.find(routeConfig => {
        return routeConfig.routeIds.includes(route.id);
      });

      if (!!routeConfig) {
        return { layerType: key, zIndex: routeConfig.zIndex }
      }

      const typeConfig = config[key].types.find(typeConfig => {
        return typeConfig.typeIds.includes(route.type);
      });
      if (!!typeConfig) {
        if (config[key].agencies.some(agency => agency.agencyIds.includes(agencyId))) {
          return { layerType: key, zIndex: typeConfig.zIndex }
        }
      }

      const agencyConfig = config[key].agencies.find(agency => agency.agencyIds.includes(agencyId));
      if (!!agencyConfig) {
        return { layerType: key, zIndex: agencyConfig.zIndex }
      }

    }


    return { layerType: 'unknown', zIndex: 1 }

  }


  private getGeometry(shape: string | string[]): SimpleGeometry {
    if (Array.isArray(shape)) {
      return new MultiLineString(shape.map(s => {
        return this.getGeometry(s) as LineString;
      }));
    } else {
      return this.polyline.readGeometry(shape, {
        dataProjection: this.dataProjection,
        featureProjection: this.featureProjection
      }) as LineString;
    }
  }

  private getAllRoutesShapes(): Observable<RouteShapeResponse> {
    const url = `@domain/@api/lines/poly`;


    let params = new HttpParams().set('types', 'ligne');
    params = params.set('reseaux', EFFECTIVE_AGENCY_IDS.join(','));

    const response = this.http.get<RouteShapeResponse>(url, { params });

    return response;
  }

}
