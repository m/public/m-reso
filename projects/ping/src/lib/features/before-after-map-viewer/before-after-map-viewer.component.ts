import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MMapModule } from '@metromobilite/m-map';
import { MapFiltersService } from '@metromobilite/m-map/m-layers';
import { FormsModule } from '@angular/forms';
import { MetaLabels, PingFeature, PingFeatureConfiguration } from '../../models';
import { PingMapService, PingService } from '../../services';
import { getRenderPixel } from 'ol/render';
import { Layer } from 'ol/layer';
import { Subject, takeUntil } from 'rxjs';
import Source from 'ol/source/Source';
import LayerRenderer from 'ol/renderer/Layer';
import { MatIconModule } from '@angular/material/icon';
import { MPingBeforeAfterLinesSource } from './map/source/before-after.source';
import { MatButtonModule } from '@angular/material/button';
import { ConfigService, FRONT_CONFIG } from '@metromobilite/m-features/core';

@Component({
  selector: 'm-ping-before-after-map-viewer',
  standalone: true,
  imports: [CommonModule, MMapModule, FormsModule, MatIconModule, MatButtonModule],
  templateUrl: './before-after-map-viewer.component.html',
  styleUrls: ['./before-after-map-viewer.component.scss']
})
export class BeforeAfterMapViewerComponent implements OnInit, OnDestroy, AfterViewInit, PingFeature {

  pingFeatureId = 'before-after-viewer';

  isDragging = false;
  dividerPosition: number = 50; // Initial position in percentage
  configuration?: PingFeatureConfiguration;
  beforeLayerIds: string[];
  afterLayerIds: string[];
  beforeLayers: Layer<Source, LayerRenderer<any>>[];
  afterLayers: Layer<Source, LayerRenderer<any>>[];

  private unsubscriber = new Subject();

  private beforeRenderHandlers: any[] = [];
  private afterRenderHandlers: any[] = [];
  metaLabels : MetaLabels;

  @ViewChild('slider') slider: ElementRef;
  @ViewChild('beforeLabel') beforeLabel: ElementRef;
  @ViewChild('afterLabel') afterLabel: ElementRef;

  constructor(private pingMapService: PingMapService, private frontConfigService : ConfigService) { }

  ngOnInit(): void {

    this.metaLabels = this.frontConfigService.config[FRONT_CONFIG.PING_RESEAU_UPDATES].meta.labels
  }
  
  ngAfterViewInit(): void {    
    this.pingMapService.showBeforeAfterLayers();
    this.pingMapService.centerToDefault();
    this.beforeLayerIds = this.configuration['beforeLayerIds'];
    this.afterLayerIds = this.configuration['afterLayerIds'];

    this.beforeLayers = this.getSelectedLayers(this.beforeLayerIds);
    this.afterLayers = this.getSelectedLayers(this.afterLayerIds);

    this.beforeLayers.forEach(layer => {
      const handler = (event: any) => this.clipBeforeLayer(event, this.dividerPosition);
      layer.on('prerender', handler);
      this.beforeRenderHandlers.push({ layer, handler });
    });

    this.afterLayers.forEach(layer => {
      const handler = (event: any) => this.clipAfterLayer(event, this.dividerPosition);
      layer.on('prerender', handler);
      this.afterRenderHandlers.push({ layer, handler });
    });

    this.beforeLayers.map(layer => {
      layer.on('postrender', (event: any) => {
        const ctx = event.context;
        ctx.restore();
      });
    });

    this.afterLayers.map(layer => {
      layer.on('postrender', (event: any) => {
        const ctx = event.context;
        ctx.restore();
      });
    });

    // Add touch event listeners directly
    const sliderElement = this.slider.nativeElement;
    
    sliderElement.addEventListener('touchstart', (event) => this.onDragStart(event), { passive: false });
    window.addEventListener('touchmove', (event) => this.onDragMove(event), { passive: false });
    window.addEventListener('touchend', (event) => this.onDragEnd(event), { passive: false });
  }

  ngOnDestroy(): void {
    this.afterLabel.nativeElement.classList.remove('active');
    this.beforeLabel.nativeElement.classList.remove('active');
    this.beforeRenderHandlers.forEach(item => {
      item.layer.un('prerender', item.handler);
    });
    this.afterRenderHandlers.forEach(item => {
      item.layer.un('prerender', item.handler);
    });

    this.pingMapService.hideBeforeAfterLayers();

    this.unsubscriber.next(true);
    this.unsubscriber.complete();

    // Remove touch event listeners
    const sliderElement = this.slider.nativeElement;
    sliderElement.removeEventListener('touchstart', (event) => this.onDragStart(event));
    window.removeEventListener('touchmove', (event) => this.onDragMove(event));
    window.removeEventListener('touchend', (event) => this.onDragEnd(event));
  }

  onDragStart(event: MouseEvent | TouchEvent): void {
    this.afterLabel.nativeElement.classList.remove('active');
    this.beforeLabel.nativeElement.classList.remove('active');
    this.isDragging = true;
    event.preventDefault();
  }

  @HostListener('window:mousemove', ['$event'])
  onDragMove(event: MouseEvent | TouchEvent): void {
    if (!this.isDragging) return;

    let clientX;
    if (event instanceof MouseEvent) {
      clientX = event.clientX;
    } else {
      clientX = event.touches[0].clientX;
    }

    const containerRect = document.querySelector('.slider-container-2').getBoundingClientRect();
    const rightBoundingBorder = containerRect.width - this.slider.nativeElement.getBoundingClientRect().width;

    // Calculate the new left position based on the mouse/touch position
    let newLeft = clientX - containerRect.left;

    // Constrain the new left position to be within the container
    newLeft = Math.max(0, newLeft);
    newLeft = Math.min(newLeft, rightBoundingBorder);

    this.dividerPosition = newLeft / containerRect.width * 100;

    const divider = document.querySelector('.divider') as HTMLElement;
    if (divider) {
      divider.style.left = `${this.dividerPosition}%`;
    }

    this.pingMapService.getMap().render();
  }

  @HostListener('window:mouseup', ['$event'])
  onDragEnd(event: MouseEvent | TouchEvent): void {
    this.isDragging = false;
  }

  moveToPosition(position: 'before' | 'after'): void {
    const containerRect = document.querySelector('.slider-container-2').getBoundingClientRect();
    const beforeLabelWidth = this.beforeLabel.nativeElement.getBoundingClientRect().width;
    const afterLabelWidth = this.afterLabel.nativeElement.getBoundingClientRect().width;

    if (position === 'before') {
      this.beforeLabel.nativeElement.classList.add('active');
      this.afterLabel.nativeElement.classList.remove('active');
      this.dividerPosition = (containerRect.width - beforeLabelWidth - 20) / containerRect.width * 100;
    } else {
      this.beforeLabel.nativeElement.classList.remove('active');
      this.afterLabel.nativeElement.classList.add('active');
      this.dividerPosition = (afterLabelWidth + 20) / containerRect.width * 100;
    }

    const divider = document.querySelector('.divider') as HTMLElement;
    if (divider) {
      divider.style.left = `${this.dividerPosition}%`;
    }
    this.pingMapService.getMap().render();
  }

  private getSelectedLayers(layerIds: string[]) {
    return this.pingMapService.getMap().getAllLayers().filter((layer: Layer) => {
      const type = layer.getProperties()['type'] || layer.getProperties()['m-map:layer:id'];
      return this.isLayerSelected(type, layerIds);
    });
  }

  private isLayerSelected(type: string, userLayerSelection: string[]): boolean {
    return !!userLayerSelection.some(layerId => layerId.includes(type));
  }

  private clipBeforeLayer(event: any, dividerPosition: number): void {
    const ctx = event.context;
    const mapSize = this.pingMapService.getMap().getSize();
    const width = mapSize[0] * (dividerPosition / 100);

    const tl = getRenderPixel(event, [0, 0]);
    const tr = getRenderPixel(event, [width, 0]);
    const bl = getRenderPixel(event, [0, mapSize[1]]);
    const br = getRenderPixel(event, [width, mapSize[1]]);

    ctx.save();
    ctx.beginPath();
    ctx.moveTo(tl[0], tl[1]);
    ctx.lineTo(bl[0], bl[1]);
    ctx.lineTo(br[0], br[1]);
    ctx.lineTo(tr[0], tr[1]);
    ctx.closePath();
    ctx.clip();
  }

  private clipAfterLayer(event: any, dividerPosition: number): void {
    const ctx = event.context;
    const mapSize = this.pingMapService.getMap().getSize();
    const width = mapSize[0] * (dividerPosition / 100);

    const tl = getRenderPixel(event, [width, 0]);
    const tr = getRenderPixel(event, [mapSize[0], 0]);
    const bl = getRenderPixel(event, [width, mapSize[1]]);
    const br = getRenderPixel(event, [mapSize[0], mapSize[1]]);

    ctx.save();
    ctx.beginPath();
    ctx.moveTo(tl[0], tl[1]);
    ctx.lineTo(bl[0], bl[1]);
    ctx.lineTo(br[0], br[1]);
    ctx.lineTo(tr[0], tr[1]);
    ctx.closePath();
    ctx.clip();
  }
}
