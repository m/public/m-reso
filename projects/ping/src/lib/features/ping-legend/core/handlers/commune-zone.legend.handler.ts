import { Injectable } from '@angular/core';
import { PingLegendHandler } from '../legend-handler.interface';
import { LinesService } from '@metromobilite/m-features/core';
import { DefaultPingLengendHandler } from './default.legend.handler';

@Injectable({ providedIn: 'root' })
export class CommuneZoneLengendHandler extends DefaultPingLengendHandler implements PingLegendHandler {
  override handlerIds = ['m-ping:commune-zone'];
  svgType = 'polygon'

  constructor(private linesService: LinesService) {
    super();
  }

  override getLegend(data: any): any {
    let legends: any[] = [];

    this.handlerIds.forEach(key => {
      data[key].map(feature => {
        const properties = feature.properties.properties;
        const styles = feature.styles;

        if (styles && styles.length > 0) {
          styles.forEach((style: any) => {
            const fillStyle = this.getFillStyle(style);
            const outlineStyle = this.getOutlineStyle(style);
            const legendItem = {
              svgType: this.svgType,
              label: properties.commune,
              fillColor: fillStyle.fillColor || 'none',
              strokeColor: outlineStyle.strokeColor || 'none',
              strokeWidth: outlineStyle.strokeWidth || 1
            };
            legends.push(legendItem);
          });
        }
      });
    });
    return legends;
  }

  override getOutlineStyle(style: any) {
    return {
      strokeColor: `rgba(${style.strokeColor.join(',')})`,
      strokeWidth: style.strokeWidth
    };
  }

  override getFillStyle(style: any) {
    return {
      fillColor: `rgba(${style.fillColor.join(',')})`
    };
  }
}
