import { Injectable } from '@angular/core';
import { PingLegendHandler } from '../legend-handler.interface';
import { DefaultPingLengendHandler } from './default.legend.handler';
import { LinesService } from '@metromobilite/m-features/core';

@Injectable({ providedIn: 'root' })
export class LineDetailLengendHandler extends DefaultPingLengendHandler implements PingLegendHandler {
  override handlerIds = ['m-layers:line'];
  svgType = 'rectangle';

  constructor(private linesService: LinesService) {
    super();
  }

  override getLegend(data: any): any {
    let legends: any[] = [];

    this.handlerIds.forEach(key => {
      const features = data[key];
      if (features.length === 1) {
        legends.push(this.createLegendItem([features[0]]));
      } else {
        const groupedByType = this.groupFeaturesByType(features);
        Object.keys(groupedByType).forEach(type => {
          if (type === 'TRAM') {
            groupedByType[type].forEach((feature: any) => {
              legends.push(this.createLegendItem([feature], false, true));
            });
          } else {
            const colors = this.getUniqueColors(groupedByType[type]);
            colors.forEach(color => {
              const featuresWithColor = groupedByType[type].filter((feature: any) => 
                this.getFillStyle(feature.styles).strokeColor === color
              );
              legends.push(this.createLegendItem(featuresWithColor, true));
            });
          }
        });
      }
    });

    return legends;
  }

  private groupFeaturesByType(features: any[]): { [type: string]: any[] } {
    return features.reduce((acc: any, feature: any) => {
      const type = this.linesService.linesAsMap[feature.properties.id].type;
      if (!acc[type]) {
        acc[type] = [];
      }
      acc[type].push(feature);
      return acc;
    }, {});
  }

  private getUniqueColors(features: any[]): string[] {
    return Array.from(new Set(features.map(feature => this.getFillStyle(feature.styles).strokeColor)));
  }

  private createLegendItem(features: any[], useTypeModeLabel: boolean = false, isTram: boolean = false): any {
    const firstFeature = features[0];
    const outlineStyle = this.getOutlineStyle(firstFeature.styles);
    const line = this.linesService.linesAsMap[firstFeature.properties.id];
    let label = '';

    if (isTram) {
      label = `Tramway ${line.shortName}`;
    } else {
      label = useTypeModeLabel ? `${line.mode} ${line.type}` : `${line.mode} ${line.type} ${line.shortName}`;
    }

    const fillColor = this.getFillStyle(firstFeature.styles).strokeColor;

    return {
      svgType: this.svgType,
      label: label,
      fillColor: fillColor,
      strokeColor: outlineStyle.strokeColor,
      strokeWidth: outlineStyle.strokeWidth
    };
  }
}

