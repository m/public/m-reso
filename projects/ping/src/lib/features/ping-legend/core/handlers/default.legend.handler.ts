import { Injectable } from '@angular/core';
import { PingLegendHandler } from '../legend-handler.interface';

@Injectable({ providedIn: 'root' })
export class DefaultPingLengendHandler implements PingLegendHandler {
  handlerIds = ['default'];

  constructor() {}

  getLegend(data: any): any {
    console.error(`No legend found for the current active layer(s)`);
    return null;
  }

  getOutlineStyle(styles: any[]): any {
    return styles.reduce((maxStyle, style) => {
      return style.strokeWidth > maxStyle.strokeWidth ? style : maxStyle;
    }, styles[0]);
  }

  getFillStyle(styles: any[]): any {
    return styles.reduce((minStyle, style) => {
      return style.strokeWidth < minStyle.strokeWidth ? style : minStyle;
    }, styles[0]);
  }
}
