import { Injectable } from '@angular/core';
import { PingLegendHandler } from '../legend-handler.interface';
import { LinesService } from '@metromobilite/m-features/core';
import { DefaultPingLengendHandler } from './default.legend.handler';

@Injectable({ providedIn: 'root' })
export class LineDiffPingLengendHandler extends DefaultPingLengendHandler implements PingLegendHandler {
  override handlerIds = ['m-ping:line-diff-visualizer'];
  svgType = 'rectangle';
  
  constructor(private linesService: LinesService) {
    super();
  }

  override getLegend(data: any): any {
    let legends: any[] = [];

    this.handlerIds.forEach(key => {
      const legendItems = data[key].map((feature: any) => {
        const outlineStyle = this.getOutlineStyle(feature.styles);
        const fillStyle = this.getFillStyle(feature.styles);
        const agencyId = feature.properties.id.split(':')[0];
        const label = agencyId === 'OLD' ? 'Ancienne' : 'Nouvelle';

        return {
          svgType: this.svgType,
          label: `${label} ligne`,
          fillColor: fillStyle.strokeColor,
          strokeColor: outlineStyle.strokeColor,
          strokeWidth: outlineStyle.strokeWidth
        };
      });
      legends = legends.concat(legendItems);
    });

    return legends;
  }
}
