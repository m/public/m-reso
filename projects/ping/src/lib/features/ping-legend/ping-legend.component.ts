import { Component, Input, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule } from '@angular/material/expansion';
import { PingService } from '@m-ping';
import { PingLegendHandler } from './core/legend-handler.interface';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subject, takeUntil } from 'rxjs';

interface LegendItem {
  id: string;
  fillColor: string;
  strokeColor: string;
  strokeWidth: number;
}

@Component({
  selector: 'm-ping-ping-legend',
  standalone: true,
  imports: [CommonModule, MatExpansionModule],
  templateUrl: './ping-legend.component.html',
  styleUrls: ['./ping-legend.component.scss']
})
export class PingLegendComponent implements OnInit, OnChanges, OnDestroy {
  @Input() activeFeatures: { [key: string]: any[] } = {};
  legends: any[] = [];
  isMobile: boolean = false;
  private unSubscriber = new Subject<void>();

  constructor(private pingService: PingService,private breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    this.updateLegends();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['activeFeatures']) {
      this.updateLegends();
    }

    this.breakpointObserver.observe(['(max-width: 770px)']).pipe(takeUntil(this.unSubscriber)).subscribe((result) => {
			this.isMobile = result.matches;
		});
  }

  ngOnDestroy(): void {
    this.unSubscriber.next();
  }

  private updateLegends(): void {
    let legendHandlers: PingLegendHandler[] = [];
    for (const layerId in this.activeFeatures) {
      const handler = this.pingService.getLegendHandler(layerId);
      legendHandlers.push(handler);
    }
  
    // Use flatMap and filter to remove undefined or null values
    this.legends = legendHandlers.flatMap(legendHandler => {
      const legends = legendHandler.getLegend(this.activeFeatures);
      return legends ? legends : [];
    });
  }
}
