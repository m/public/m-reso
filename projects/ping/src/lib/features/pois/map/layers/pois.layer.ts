import { Injectable } from '@angular/core';
import { LayerInputConfig, MapComponent, MapLayer, MapLayerBase } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { PingClustersSource } from '../source/pois.source';
import { MPingCircleStyle } from '../style/circle.style';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:pois')
export class PingClustersLayer extends MapLayerBase {

  constructor(
    private pingSource: PingClustersSource,
    private circleStyle: MPingCircleStyle
  ) { super(); }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    return new VectorLayer({
      source: this.pingSource.getSource() as VectorSource,
      style: (feature: Feature, resolution: number) => {

        if (feature.get('visibleMap') == false) {
          return null;
        }

        const useDark = component.tiles
          .filter((config) => config.visible)
          .reduce((_, config) => config.layer.includes('dark'), true);

        const config = {
          color: useDark ? '#000' : '#FFF',
          radius: 8,
          strokeWidth: 4,
          strokeColor: useDark ? '#D9DADB' : '#000',
          zIndex: 100
        }

        if (feature.get('searchedCluster')) {
          config.radius = 10;
          config.strokeWidth = 4;
        } else {
          if (!!feature.get('fillColor')) {
            config.color = `#${feature.get('fillColor')}`;
            config.strokeColor = `#${feature.get('strokeColor')}`;
          }


          if (resolution > 15) {
            config.radius = 6;
            config.strokeWidth = 2;
          }

          if (resolution > 30) {
            config.radius = 3;
            config.strokeWidth = 1;
          }

          const isSelected = feature.get('selected') || false;
          if (isSelected) {
            config.radius = 12;
            config.strokeWidth = 6;

            if (resolution > 10) {
              config.radius = 8;
              config.strokeWidth = 4;
            }

            if (resolution > 30) {
              config.radius = 5;
              config.strokeWidth = 2;
            }
          }

        }


        return this.circleStyle.build(config);
      }
    });
  }
}
