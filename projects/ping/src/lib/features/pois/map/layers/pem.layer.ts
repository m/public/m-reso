import { Injectable } from '@angular/core';
import { LayerInputConfig, MapComponent, MapLayer, MapLayerBase } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { PingClustersSource } from '../source/pois.source';
import { MPingCircleStyle } from '../style/circle.style';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:pems')
export class PemsLayer extends MapLayerBase {

  constructor (
    private pingSource: PingClustersSource,
    private circleStyle: MPingCircleStyle
  ) { super(); }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {
    return new VectorLayer({
      source: this.pingSource.getSource() as VectorSource,
      style: (feature: Feature, resolution: number) => {
        

        if (feature.get('visibleMap') == false) {
          return null;
        }

        if (!feature.get('pem')) {
          return null;
        }

        const config = {
          color: `#000`,
          radius: 14,
          strokeWidth: 10,
          strokeColor: "#D9DADB",
          zIndex: 100
        }

        return this.circleStyle.build(config);
      }
    });
  }
}
