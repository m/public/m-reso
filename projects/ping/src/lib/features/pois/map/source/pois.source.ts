import { Inject, Injectable } from '@angular/core';
import { ClusterData } from '@m-ping';
import { ConfigService, FRONT_CONFIG, TypesHelper } from '@metromobilite/m-features/core';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import { Extent } from 'ol/extent';
import Point from 'ol/geom/Point';
import { transform, transformExtent } from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import { bbox } from 'ol/loadingstrategy';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PointsResponse } from '@metromobilite/m-map/m-layers';

@Injectable({ providedIn: 'root' })
export class PingClustersSource extends MapSourceBase {
  override source: VectorSource;
  private compRes: any;
  private compRej: any;
  complete = new Promise((res, rej) => {
    this.compRes = res;
    this.compRej = rej;
  });

  constructor (
    private http: HttpClient,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    private frontConfigService: ConfigService
  ) {
    super(); this.source = new VectorSource({});
    const highlightedClusters = this.frontConfigService.config[FRONT_CONFIG.PING_RESEAU_UPDATES].highlightedClusters.clusterList;

    this.source = new VectorSource({
      strategy: bbox,
      loader: async (extent, resolution, projection) => {
        const tExtent = transformExtent(extent, projection, this.dataProjection);
        const params = new HttpParams({
          fromObject: {
            xmin: `${tExtent[0]}`.replace('Infinity', '180'),
            ymin: `${tExtent[1]}`.replace('Infinity', '180'),
            xmax: `${tExtent[2]}`.replace('Infinity', '180'),
            ymax: `${tExtent[3]}`.replace('Infinity', '90'),
            types: ['clusters'],
            epci: 'All',
            includeHiddenAgencies: true
          },
        });


        this.http.get<PointsResponse>(`@domain/@api/points/json`, { params }).subscribe(response => {
          for (const responseFeature of response.features) {

            const id = responseFeature.properties['code'];
            const agencyId = id.split(':')[0];

            if (['BUL', 'MCO', 'FUN', 'SNC'].includes(agencyId)) continue;

            let feature = this.source.getFeatureById(id);

            if (!feature) {
              const geometry = new Point(transform(responseFeature.geometry.coordinates, this.dataProjection, this.featureProjection));
              feature = new Feature({
                geometry,
                ...responseFeature.properties,
              });
              feature.setId(id);
              feature.set('visibleMap', false);
              this.push(feature);
            }
          }
          this.compRes();
        });
      },
    });
  }

  push(...features: Feature[]): void {
    this.source.addFeatures(features);
  }

  remove(feature: Feature): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature | undefined {
    return this.source.getFeatureById(id);
  }

  getExtent(): Extent {
    return this.source.getExtent();
  }

  activateFeatures(clusterDataArray: ClusterData[], lineTextColor: string, lineColor: string, isOldLine: boolean) {
    this.setFeaturesVisibility(false);
    clusterDataArray.map(data => this.activateFeatureFromClusterData(data, lineTextColor, lineColor, isOldLine));
  }

  activateSearchedCluster(data: ClusterData, lineTextColor: string, lineColor: string, isOldLine: boolean) {
    this.setFeaturesVisibility(false);

    let feature = this.source.getFeatureById(data.code);

    if (!!feature) {
      feature.set('visibleMap', true);
      feature.set('searchedCluster', true);
      feature.set('fillColor', "000000");
      feature.set('strokeColor', "D9DADB");
    } else {
      this.createFeatureFromClusterData(data, lineTextColor, lineColor, isOldLine);
      let feature = this.source.getFeatureById(data.code);
      feature.set('searchedCluster', true);
    }
  }



  activateFeatureFromClusterData(data: ClusterData, lineTextColor: string, lineColor: string, isOldLine: boolean) {
    let feature = this.source.getFeatureById(data.code);
    if (!!feature) {
      const lineColorConfig = isOldLine ? "D9DADB" : lineColor;
      feature.set('visibleMap', true);
      feature.set('fillColor', lineTextColor);
      feature.set('strokeColor', lineColorConfig);
    } else {
      this.createFeatureFromClusterData(data, lineTextColor, lineColor, isOldLine);
    }
  }

  createFeatureFromClusterData(data: ClusterData, lineTextColor: string, lineColor: string, isOldLine: boolean): void {
    const coordinate = [data.lon, data.lat];
    if (!coordinate) {
      return null;
    }

    const geometry = new Point(transform(coordinate, this.dataProjection, this.featureProjection));
    const feature = new Feature({
      code: data.code,
      city: data.city,
      name: data.name,
      visible: data.visible,
      geometry: geometry
    });

    const lineColorConfig = isOldLine ? "D9DADB" : lineColor;

    feature.setId(data.code);
    feature.set('visibleMap', true);
    feature.set('fillColor', lineTextColor);
    feature.set('strokeColor', lineColorConfig);
    this.push(feature);
    this.source.changed();
  }


  setFeaturesVisibility(visibility: boolean): void {
    this.source.getFeatures().forEach(feature => {
      feature.set('pem', false);
      feature.set('searchedCluster', false);
      feature.set('visibleMap', visibility);
    });
    this.source.changed();
  }
}
