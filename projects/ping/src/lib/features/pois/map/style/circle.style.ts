import { Injectable } from '@angular/core';
import { AbstractStyle } from '@metromobilite/m-map';
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Circle from 'ol/style/Circle';
import { CircleStyleConfig } from '@metromobilite/m-map/m-layers';
import Text from 'ol/style/Text';

@Injectable({ providedIn: 'root' })
export class MPingCircleStyle extends AbstractStyle {

  protected makeStyle(config: CircleStyleConfig & { text?: string }): Style | Style[] {
    const textConfig = config.text ? new Text({
      text: config.text,
      font: '10px Arial',
      fill: new Fill({
        color: '#000000'
      }),
      stroke: new Stroke({
        color: '#FFF',
        width: 1
      }),
      offsetY: -10
    }) : undefined;

    return new Style({
      image: new Circle({
        radius: config.radius || 3,
        fill: new Fill({
          color: config.color
        }),
        stroke: new Stroke({
          color: config.strokeColor || '#000',
          width: config.strokeWidth || 1
        })
      }),
      // text: textConfig,
      zIndex: config.zIndex || 1,
    });
  }

}
