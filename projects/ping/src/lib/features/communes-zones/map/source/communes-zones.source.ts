import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Extent } from 'ol/extent';

@Injectable({ providedIn: 'root' })
export class CommuneZoneSource extends MapSourceBase {

    zoneSource = new VectorSource();
    private geoJSON = new GeoJSON();
    private cache: Map<string, Feature<Geometry>> = new Map();
    private _selectedFeature: Feature<Geometry> | null = null;
    public get selectedFeature(): Feature<Geometry> | null {
        return this._selectedFeature;
    }
    public set selectedFeature(value: Feature<Geometry> | null) {
        this._selectedFeature = value;
    }

    constructor(
        private http: HttpClient,
        @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
        @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    ) {
        super();
    }

    override push(...features: Feature<Geometry>[]): void {
        this.zoneSource.addFeatures(features);
    }

    override remove(feature: Feature<Geometry>): void {
        this.zoneSource.removeFeature(feature);
    }

    override find(id: string): Feature<Geometry> | null {
        return this.zoneSource.getFeatureById(id);
    }

    override changed(): void {
        this.zoneSource.changed();
    }

    clearFeatures(): void {
        this.zoneSource.clear();
    }

    getData(code: string): Observable<boolean> {
        const cachedFeature = this.cache.get(code);
        if (cachedFeature) {
            this.updateSelectedFeature(cachedFeature);
            return of(true);
        } else {
            return this.http.get<any>(`@domain/@api/polygons/json?types=communes&codes=${code}`).pipe(
                map(response => {
                    const features = response.features.map(f => {
                        const geometry = this.geoJSON.readGeometry(f.geometry, {
                            dataProjection: this.dataProjection,
                            featureProjection: this.featureProjection
                        });
                        const feature = new Feature({
                            properties: f.properties,
                            geometry,
                        });
                        feature.setId(f.properties.code);
                        this.cache.set(f.properties.code, feature); // Cache the feature
                        return feature;
                    });
                    if (features.length > 0) {
                        const newFeature = features[0];
                        this.updateSelectedFeature(newFeature);
                    }
                    return true;
                }),
                catchError(err => {
                    console.error('Error fetching data', err);
                    return of(false);
                })
            );
        }
    }

    updateSelectedFeature(newFeature: Feature<Geometry>) {
        if (this.selectedFeature) {
            this.selectedFeature.set('selected', false);
        }
        newFeature.set('selected', true);
        this.selectedFeature = newFeature;
        this.clearFeatures();
        this.push(newFeature);
        this.changed();
    }

    getExtent(): Extent | null {
        const features = this.zoneSource.getFeatures();
        if (features.length > 0) {
            return this.zoneSource.getExtent();
        } else {
            return null;
        }
    }
}
