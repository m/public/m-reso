import { Injectable } from '@angular/core';
import { LayerInputConfig, MapComponent, MapLayer, MapLayerBase } from '@metromobilite/m-map';
import Base from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import { CommuneZoneSource } from '../source/communes-zones.source';


@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:commune-zone')
export class CommuneZoneLayer extends MapLayerBase {

  constructor(public override source: CommuneZoneSource) { super(); }

  buildLayer(component: MapComponent, config: LayerInputConfig): Base {


    return new VectorLayer({
      source: this.source.zoneSource,
      style: (feature, resolution) => {

        if(!feature.get('selected')) return null;

        const useDark = component.tiles
          .filter((config) => config.visible)
          .reduce((_, config) => config.layer.includes('dark'), true);

        const zoneStyle = new Style({
          stroke: new Stroke({
            color: useDark ? [180, 155, 218, 1] : [129, 84, 192, 1],
            lineDash: [10, 10],
            lineCap: 'square',
            width: 6,
          }),
          fill: new Fill({
            color: useDark ? [129, 84, 192, 0.15] : [129, 84, 192, 0.15]
          })
        });
        return zoneStyle;
      }
    });
  }

}