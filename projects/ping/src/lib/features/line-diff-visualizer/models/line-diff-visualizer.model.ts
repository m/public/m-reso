export interface RouteData {
  id: string,
  gtfsId: string,
  shortName: string,
  longName: string,
  color: string,
  textColor: string,
  mode: RouteMode,
  type: RouteType | string
}

export enum RouteType {
  TRAM = 'TRAM',
  CHRONO = 'CHRONO',
  PROXIMO = 'PROXIMO',
  FLEXO = 'FLEXO',
  NAVETTE = 'NAVETTE',
  C38 = 'C38',
  C38_STRUCT = 'C38_STRUCT',
  C38_AUTRE = 'C38_AUTRE',
  Structurantes = 'Structurantes',
  Secondaires = 'Secondaires',
  TAD = 'TAD',
  PMR = 'PMR',
  Interurbaines = 'Interurbaines',
  Urbaines = 'Urbaines',
  SCOL = 'SCOL',
  SNC = 'SNC'
}

export enum RouteMode {
  TRAM = 'TRAM',
  SUBWAY = 'SUBWAY',
  RAIL = 'RAIL',
  BUS = 'BUS',
  FERRY = 'FERRY',
  CABLE_CAR = 'CABLE_CAR',
  GONDOLA = 'GONDOLA',
  FUNICULAR = 'FUNICULAR',
  TROLLEYBUS = 'TROLLEYBUS',
  MONORAIL = 'MONORAIL',
  AIRPLANE = 'AIRPLANE',
  TAXI = 'TAXI',
  CAR = 'CAR'
}

export interface ClusterData {
  id: string
  code: string
  city: string
  name: string
  visible: boolean
  lat: number
  lon: number
}

export interface RouteShapeResponse {
  type: string;
  layer?: string;
  features: RouteShapeFeature[];
}

export interface RouteShapeFeature {
  properties: RouteShapeProperties;
}

export interface RouteShapeProperties {
  PMR: number;
  CODE: string;
  type: string;
  id: string;
  shape: string[];
}

export enum PingLayerIds {
  POIS = '',
  LINE_DIFF_VISUALIZER = '',
  BEFORE = '',
  AFTER = ''
}
