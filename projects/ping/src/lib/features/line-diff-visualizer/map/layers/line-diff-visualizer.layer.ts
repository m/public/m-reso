import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayerBase, MapComponent, MapLayer } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { LineDiffVisualizerSource } from '../source/line-diff-visualizer.source';
import { LineDiffVisualizerStyle } from '../style/line-diff-visualizer.style';
import { LineStyleConfig } from '@metromobilite/m-map/m-layers';
import { hexToRGBA } from '@m-ping';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:line-diff-visualizer')
export class LineDiffVisualizerLayer extends MapLayerBase {

  constructor (
    public override source: LineDiffVisualizerSource,
    private lineStyle: LineDiffVisualizerStyle
  ) {
    super();
  }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {
        const isOldLine = feature.get('isOldLine');
        const type = feature.get('type');

        // Condition to filter out features based on type and old/new line distinction
        if ((isOldLine && config['type'] && config['type'] !== 'old') || (!isOldLine && config['type'] && config['type'] !== 'new')) {
          return [];
        }

        // Adjust the style settings based on whether it is an old or new line
        const lineStyleConfig: LineStyleConfig = {
          color: feature.get('color'),
          backgroundColor: isOldLine ? '#373636' : (config['backgroundColor'] || '#000'),
          noBorder: isOldLine ? false : true,
          width: isOldLine ? 4 : 12,
          zOffset: isOldLine ? 3 : 1
        };


        const isActive = feature.get('activated');
        if (isActive) {
          lineStyleConfig.color = hexToRGBA(feature.get('color'), 1);
          if (isOldLine) {
            // lineStyleConfig.width = 4;
            lineStyleConfig.color = '#E6E7E7';
          }
        } else {
          // lineStyleConfig.zOffset = 1;
          lineStyleConfig.color = hexToRGBA(feature.get('color'), isOldLine ? 1 : 0.5);
        }
        return this.lineStyle.build(lineStyleConfig);
      }
    });
    return layer;
  }
}
