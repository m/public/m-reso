import { Injectable } from '@angular/core';
import { AbstractStyle } from '@metromobilite/m-map';
import { LineStyleConfig } from '@metromobilite/m-map/m-layers';
import Stroke, { Options as StrokeOptions } from 'ol/style/Stroke';
import Style from 'ol/style/Style';

@Injectable({ providedIn: 'root' })
export class LineDiffVisualizerStyle extends AbstractStyle {

  protected makeStyle(config: LineStyleConfig & { mode?: 'dashed' | 'grey' | 'partially-transparent' }): Style | Style[] {
    const width = config.width || 3;
    const zOffset = config.zOffset || 0;
    const styles = [];

    // Default stroke options
    let strokeOptions: StrokeOptions = {
      color: config.color,
      width,
      lineCap: 'round',
      lineJoin: 'round',
    };

    // Apply mode-specific styles
    switch (config.mode) {
      case 'dashed':
        strokeOptions = {
          ...strokeOptions,
          lineDash: [25, 25, 25, 25],
          lineDashOffset: 0,
          lineCap: 'square',
        };
        break;
      case 'grey':
        strokeOptions = {
          ...strokeOptions,
          color: '#95989A',
        };
        break;
      case 'partially-transparent':
        const colorString = config.color as string;
        // Assuming the color is in the format '#rrggbb', convert it to 'rgba(r, g, b, opacity)'
        const rgba = `rgba(${parseInt(colorString.slice(1, 3), 16)}, ${parseInt(colorString.slice(3, 5), 16)}, ${parseInt(colorString.slice(5, 7), 16)}, 0.5)`; // 0.5 is the opacity
        strokeOptions = {
          ...strokeOptions,
          color: rgba,
        };
        break;
    }

    // Add border style if needed
    if (!config.noBorder) {
      styles.push(new Style({
        stroke: new Stroke({
          ...strokeOptions,
          color: config.backgroundColor || strokeOptions.color,
          width: width + 2
        }),
        zIndex: 3 + zOffset,
      }));
    }

    // Add main line style
    styles.push(new Style({
      stroke: new Stroke(strokeOptions),
      zIndex: 3 + zOffset,
    }));

    // Add alternate color style if needed
    if (config.alternateColor && config.lineDash && config.lineDashOffset) {
      styles.push(new Style({
        stroke: new Stroke({
          color: config.alternateColor,
          width,
          lineDash: config.lineDash,
          lineDashOffset: config.lineDashOffset,
        }),
        zIndex: 2 + zOffset,
      }));
    }

    return styles;
  }
}
