import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Feature } from 'ol';
import SimpleGeometry from 'ol/geom/SimpleGeometry';
import VectorSource from 'ol/source/Vector';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapSourceBase } from '@metromobilite/m-map';
import { Geometry } from 'ol/geom';
import { RouteData, RouteShapeResponse } from '../../models/line-diff-visualizer.model';
import MultiLineString from 'ol/geom/MultiLineString';
import LineString from 'ol/geom/LineString';
import Polyline from 'ol/format/Polyline';
import { LinesService } from '@metromobilite/m-features/core';
import { Extent } from 'ol/extent';


@Injectable({ providedIn: 'root' })
export class LineDiffVisualizerSource extends MapSourceBase {


  override source: VectorSource;
  private polyline: Polyline;

  constructor (private http: HttpClient, @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string, @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string, private lineServices: LinesService) {
    super();
    this.source = new VectorSource({});
    this.polyline = new Polyline({});
  }


  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getExtent(): Extent {
    return this.source.getExtent();
  }

  clear(): void {
    this.source.clear();
  }

  /**
   * Fetches and processes line data for both new and old lines.
   * @param newLineId The ID of the new line.
   * @param oldLineId The ID of the old line.
   */
  fetchLineData(oldLineId: string, newLineId: string): Observable<boolean> {
    //TODO: work on better loading strategy
    return forkJoin({
      routes: this.fetchRoutes([oldLineId, newLineId]),
      shapes: this.fetchShapes([oldLineId, newLineId])
    }).pipe(
      map(({ routes, shapes }) => {
        routes.forEach(route => {
          const shapeFeature = shapes.features.find(feature => feature.properties.id.replace('_', ':') === route.id);
          if (shapeFeature) {
            const isOldLine = !route.id.startsWith('N');
            this.createFeature(route, shapeFeature.properties.shape, isOldLine);
          }
        });
        return true;
      }),
      catchError(err => {
        console.error('Error fetching line data:', err);
        return of(false);
      })
    );
  }

  private fetchRoutes(lineIds: string[]): Observable<RouteData[]> {
    const url = `@domain/@api/routers/default/index/routes`;
    return this.http.get<RouteData[]>(url, {
      params: new HttpParams().set('codes', lineIds.join(',')).set('allRoutes', 'true')
    });
  }

  private fetchShapes(lineIds: string[]): Observable<RouteShapeResponse> {
    const url = `@domain/@api/lines/poly`;
    return this.http.get<RouteShapeResponse>(url, {
      params: new HttpParams().set('types', 'ligne').set('codes', lineIds.join(','))
    });
  }

  private createFeature(route: RouteData, shape: string[], isOldLine: boolean): void {
    let geometry = this.getGeometry(shape);

    const feature = new Feature({
      geometry,
      type: route.type,
      color: isOldLine ? '#DBDBDB' : `#${route.color}`,
      textColor: `#${route.textColor}`,
      isOldLine,
      activated: isOldLine? false : true
    });
    feature.setId(route.id);
    this.source.addFeature(feature);
  }

  private getGeometry(shape: string | string[]): SimpleGeometry {
    if (Array.isArray(shape)) {
      return new MultiLineString(shape.map(s => {
        return this.getGeometry(s) as LineString;
      }));
    } else {
      return this.polyline.readGeometry(shape, {
        dataProjection: this.dataProjection,
        featureProjection: this.featureProjection
      }) as LineString;
    }
  }
}
