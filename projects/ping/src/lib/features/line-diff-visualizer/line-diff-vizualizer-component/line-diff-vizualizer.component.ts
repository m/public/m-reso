import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PingFeature, PingFeatureConfiguration } from '../../../models';
import { LineDiffVisualizerSource } from '../map/source/line-diff-visualizer.source';
import { PingMapService } from '@m-ping';
import { PingClustersSource } from '@m-ping/features/pois/map/source/pois.source';
import * as olExtent from 'ol/extent';

@Component({
  selector: 'm-ping-line-diff-vizualizer',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './line-diff-vizualizer.component.html',
  styleUrls: ['./line-diff-vizualizer.component.scss']
})
export class LineDiffVizualizerComponent {
  pingFeatureId = 'line-diff-visualizer';
  configuration?: PingFeatureConfiguration;

  constructor (private lineDiffVisualizerSource: LineDiffVisualizerSource, private pingClustersSource: PingClustersSource, private pingMapService: PingMapService) { }

  ngOnInit() {
    this.lineDiffVisualizerSource.fetchLineData(this.configuration['oldLine'], this.configuration['newLine']).subscribe(result => {
      this.pingMapService.showLineLayers();
      this.pingMapService.fitView(this.lineDiffVisualizerSource.getExtent(), 0);
      // this.fitToLargerExtent();
    }, error => {
      console.error('Error fetching data:', error);
    });
  }



  private extend(): olExtent.Extent {
    return olExtent.extend(this.lineDiffVisualizerSource.getExtent(), this.lineDiffVisualizerSource.getExtent());
  }

  private fitToLargerExtent() {
    const extent1 = this.pingClustersSource.getExtent();
    const extent2 = this.lineDiffVisualizerSource.getExtent();
    const area1 = this.calculateExtentArea(extent1);
    const area2 = this.calculateExtentArea(extent2);

    if (area1 > area2) {
      this.pingMapService.fitView(extent1, 0);
    } else {
      this.pingMapService.fitView(extent2, 0);
    }
  }

  private calculateExtentArea(extent: olExtent.Extent): number {
    return olExtent.getWidth(extent) * olExtent.getHeight(extent);
  }

}
