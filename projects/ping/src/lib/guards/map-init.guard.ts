import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { PingMapService } from '../services/ping-map.service';

@Injectable({
  providedIn: 'root'
})
export class MapInitGuard implements CanActivate {
  path: ActivatedRouteSnapshot[]; route: ActivatedRouteSnapshot;

  constructor (private pingMapService: PingMapService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.pingMapService.initialized) {
      return true;
    }
    return new Promise((resolve) => {
      this.pingMapService.initMap.subscribe((isInit: boolean) => {
        if (isInit) resolve(true);
      });
    });
  }
}
